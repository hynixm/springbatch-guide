package com.apress.batch.chapter9;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@EnableBatchProcessing
@SpringBootApplication
public class Chapter9Application {

	public static void main(String[] args) {
		/*List<String> newArgs = new ArrayList<>(3);
		newArgs.add("customerFile=/data/customerWithEmail.csv");
		newArgs.add("outputFile=file:/tmp/customer.xml");
		SpringApplication.run(Chapter9Application.class, newArgs.toArray(new String[0]));*/

		ApplicationContext ctx = SpringApplication.run(Chapter9Application.class, args);
		/*String[] beanNames = ctx.getBeanDefinitionNames();
        System.out.println("beanSize:" + beanNames.length);
        int i = 0;
        for(String beanName : beanNames){
            System.out.println(String.format("%d  %s",++i,beanName));
        }*/
	}
}
