package com.apress.batch.chapter9.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//@RestController 是@controller和@ResponseBody 的结合
//@ResponseBody 它的作用简短截说就是指该类中所有的API接口返回的数据，甭管你对应的方法返回Map或是其他Object，它会以Json字符串的形式返回给客户端
@RestController
@RequestMapping("/springbatch9")
public class JobController {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;


    /**
     * 灵活参数，名称映射
     * @RequestParam（）指定的参数可以是普通元素、数组、集合、对象等等
     */
    @RequestMapping(value = "/{jobName}/map/run")
    public ExitStatus runJobMapParamAutoAdd(@PathVariable("jobName") String jobName, @RequestParam Map<String,String> params) throws Exception {
        // 组件方法获取job
        Job job = this.context.getBean(jobName, Job.class);
        Map<String, JobParameter> parameterMap = new HashMap<>();

        for(String key : params.keySet()){//
            parameterMap.put(key, new JobParameter(params.get(key), true));//true 识别性参数
        }
        // 参数构建器，构建springbatch的job参数
        JobParameters jobParameters =
                new JobParametersBuilder(new JobParameters(parameterMap),
                        this.jobExplorer)
                        .toJobParameters();
        return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }
}
