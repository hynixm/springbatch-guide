import com.alibaba.druid.pool.DruidDataSource;
import com.bocd.mapper.second.dao.BaseSqlDao;
import com.bocd.mapper.primary.dao.BaseTabDealJobDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.bocd.SpringBatchApplication.class)
public class SpringRunTest {
    @Autowired
    @Qualifier("secondDataSource")
    private DataSource secondDataSource;

    @Autowired
    private BaseSqlDao baseSqlDao;

    @Autowired
    private BaseTabDealJobDao baseTabDealJobDao;


    @Test
    public void test1(){
        baseSqlDao.mapperTest();
        System.out.println(secondDataSource instanceof DruidDataSource);
        DruidDataSource ds = (DruidDataSource) this.secondDataSource;
        System.out.println(ds.isTestWhileIdle());
        System.out.println(ds.getValidationQuery());

    }
}
