import com.bocd.entity.dto.FmCcyRateTemp;
import org.junit.Test;

import java.lang.reflect.Field;

public class UnitTest {

    @Test
    public void test(){
        FmCcyRateTemp fmCcyRateTemp = new FmCcyRateTemp();
        Class<? extends FmCcyRateTemp> aClass = fmCcyRateTemp.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        StringBuilder sf = new StringBuilder();
        for(Field field : declaredFields){
            sf.append(field.getName() + ",");
        }
        System.out.println(sf.toString());
    }
}
