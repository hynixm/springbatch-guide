package com.bocd.controller;

import com.bocd.job.base.BatchJob;
import com.bocd.job.base.GlobalDataDetectionUtil;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//@RestController 是@controller和@ResponseBody 的结合
//@ResponseBody 它的作用简短说就是指该类中所有的API接口返回的数据，甭管你对应的方法返回Map或是其他Object，它会以Json字符串的形式返回给客户端
@RestController
@RequestMapping("/springbatch")
public class JobController {
    @Autowired
    private JobLauncher myJobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;


    /**
     * 灵活参数，名称映射
     * @RequestParam（）指定的参数可以是普通元素、数组、集合、对象等等
     */
    @RequestMapping(value = "/{jobName}/single")
    public String singleJob(@PathVariable("jobName") String jobName, @RequestParam Map<String,Object> params) throws Exception {

        // 同名称的job返回的是同一个 initObj 对象
        GlobalDataDetectionUtil initObj = GlobalDataDetectionUtil.initGlobalDataDetectionObj(jobName);
        BatchStatus batchStatus = initObj.getResultT().get(jobName);
        if(batchStatus != null && batchStatus.isRunning()){
            return "已存在一个job[" + jobName + "]在执行中了,请不要同时重复执行....";
        }

        //输入参数都作为job的识别性参数，可以在batch表里清晰的查看情况细节
        //相同jobName的作业，第2次执行，如果输入识别性参赛不去覆盖第1次的，默认会把第1次执行的参数kv作为第2次执行的识别性参数
        Map<String, JobParameter> parameterMap = new HashMap<>();
        for(String key : params.keySet()){//
            parameterMap.put(key, new JobParameter(params.get(key).toString(), true));//true 识别性参数
        }

        // 组件方法获取job
        Job job = this.context.getBean(jobName, BatchJob.class).getJob(params,initObj);
        // 参数构建器，构建springbatch的job参数
        JobParameters jobParameters =
                new JobParametersBuilder(new JobParameters(), this.jobExplorer)
                        //.getNextJobParameters(job)
                        .toJobParameters();
        /**
         * 多线程并发，锁作用：让http请求执行job，job执行完成后http请求再返回结果
         * 1. http1线程，另起一个job1线程执行作业
         * 2. initObj.getResultStatus(jobName)：这里同步方法中wait,即http1线程挂起（释放initObj对象上的锁）
         * 3. job1线程： job监听后置执行initObj对象（与http1线程中是同一个对象）同步方法的notifyAll，通知http1继续执行
         */
        JobExecution run = this.myJobLauncher.run(job, jobParameters);
        // 这里把这个http线程挂起
        // 等待job执行到最后，即job中的监听后置方法 SingleJobListener#afterJob , 调用 initObj.setResultStatus 方法,释放锁，这里会才会继续往下执行到http请求返回，线程结束。
        initObj.getResultStatus(jobName);

        return run.getExitStatus().toString();
    }

    /**
     * 灵活参数，名称映射
     * @RequestParam（）指定的参数可以是普通元素、数组、集合、对象等等
     */
    @RequestMapping(value = "/{jobName}/run")
    public String runJob(@PathVariable("jobName") String jobName, @RequestParam Map<String,Object> params) throws Exception {

        // 同名称的job返回的是同一个 initObj 对象
        GlobalDataDetectionUtil initObj = GlobalDataDetectionUtil.initGlobalDataDetectionObj(jobName);
        BatchStatus batchStatus = initObj.getResultT().get(jobName);
        if(batchStatus != null && batchStatus.isRunning()){
            return "已存在一个job[" + jobName + "]在执行中了,请不要同时重复执行....";
        }

        //输入参数都作为job的识别性参数，可以在batch表里清晰的查看情况细节
        //相同jobName的作业，第2次执行，如果输入识别性参赛不去覆盖第1次的，默认会把第1次执行的参数kv作为第2次执行的识别性参数
        Map<String, JobParameter> parameterMap = new HashMap<>();
        parameterMap.put("dataDate", new JobParameter(params.get("dataDate").toString(), true));//true 识别性参数

        // 组件方法获取job
        Job job = this.context.getBean(jobName, BatchJob.class).getJob(params,initObj);
        // 参数构建器，构建springbatch的job参数
        JobParameters jobParameters = null;
        if(params.containsKey("reRun") && "true".equals(params.get("reRun").toString())){
            jobParameters =
                    new JobParametersBuilder(new JobParameters(parameterMap), this.jobExplorer)
                            .getNextJobParameters(job)
                            .toJobParameters();
        }else{
            jobParameters =
                    new JobParametersBuilder(new JobParameters(parameterMap), this.jobExplorer)
                            .toJobParameters();
        }

        /**
         * 多线程并发，锁作用：让http请求执行job，job执行完成后http请求再返回结果
         * 1. http1线程，另起一个job1线程执行作业
         * 2. initObj.getResultStatus(jobName)：这里同步方法中wait,即http1线程挂起（释放initObj对象上的锁）
         * 3. job1线程： job监听后置执行initObj对象（与http1线程中是同一个对象）同步方法的notifyAll，通知http1继续执行
         */
        JobExecution run = this.myJobLauncher.run(job, jobParameters);
        // 这里把这个http线程挂起
        // 等待job执行到最后，即job中的监听后置方法 SingleJobListener#afterJob , 调用 initObj.setResultStatus 方法,释放锁，这里会才会继续往下执行到http请求返回，线程结束。
        initObj.getResultStatus(jobName);

        return run.getExitStatus().toString();
    }
}
