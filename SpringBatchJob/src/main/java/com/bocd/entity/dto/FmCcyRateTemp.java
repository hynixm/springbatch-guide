package com.bocd.entity.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FmCcyRateTemp implements Serializable {
    private String ccy;

    private String branch;

    private String rateType;

    private String effectiveDate;

    private String lastChangeDate;

    private String quoteType;

    private BigDecimal ccyRate;

    private BigDecimal buyRate;

    private BigDecimal sellRate;

    private BigDecimal centralBankRate;

    private BigDecimal buySpread;

    private BigDecimal sellSpread;

    private BigDecimal notesBuyRate;

    private BigDecimal notesSellRate;

    private static final long serialVersionUID = 1L;

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getLastChangeDate() {
        return lastChangeDate;
    }

    public void setLastChangeDate(String lastChangeDate) {
        this.lastChangeDate = lastChangeDate;
    }

    public String getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(String quoteType) {
        this.quoteType = quoteType;
    }

    public BigDecimal getCcyRate() {
        return ccyRate;
    }

    public void setCcyRate(BigDecimal ccyRate) {
        this.ccyRate = ccyRate;
    }

    public BigDecimal getBuyRate() {
        return buyRate;
    }

    public void setBuyRate(BigDecimal buyRate) {
        this.buyRate = buyRate;
    }

    public BigDecimal getSellRate() {
        return sellRate;
    }

    public void setSellRate(BigDecimal sellRate) {
        this.sellRate = sellRate;
    }

    public BigDecimal getCentralBankRate() {
        return centralBankRate;
    }

    public void setCentralBankRate(BigDecimal centralBankRate) {
        this.centralBankRate = centralBankRate;
    }

    public BigDecimal getBuySpread() {
        return buySpread;
    }

    public void setBuySpread(BigDecimal buySpread) {
        this.buySpread = buySpread;
    }

    public BigDecimal getSellSpread() {
        return sellSpread;
    }

    public void setSellSpread(BigDecimal sellSpread) {
        this.sellSpread = sellSpread;
    }

    public BigDecimal getNotesBuyRate() {
        return notesBuyRate;
    }

    public void setNotesBuyRate(BigDecimal notesBuyRate) {
        this.notesBuyRate = notesBuyRate;
    }

    public BigDecimal getNotesSellRate() {
        return notesSellRate;
    }

    public void setNotesSellRate(BigDecimal notesSellRate) {
        this.notesSellRate = notesSellRate;
    }
}