package com.bocd.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static boolean isEmpty(Object object) {
		return null == object || "".equals(object.toString());
	}

	public static boolean isTrimEmpty(Object object){
		return null == object || "".equals(object.toString().trim());
	}

	public static boolean isNumber(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	public static String toString(Object object) {
		return null == object ? "" : object.toString();
	}

}
