package com.bocd.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 公共工具类
 * @author Qh
 * @version 1.0
 * @date 2020/11/2
 */

public final class CommonUtils {
    private static Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    public static Map<String, String> fastToMap(String keys, String values) {
        if (StringUtils.isEmpty(keys)) {
            return new HashMap<>();
        }
        Map map =new HashMap();
        String[] key = keys.split("\\|\\@\\|", -1);
        String[] value = values.split("\\|\\@\\|", -1);
        for(int i=0;i<key.length;i++) {
            map.put(key[i], value[i]);
        }
        return map;
    }


     /**
      * @param time:需要转换的日期字符串，format:转换的格式
      * @return Date:按一定格式转换好的日期类型
      * @description 将日期字符串按指定格式转换为日期类型，如果format类型为TIMESTAMP，则获取当前时间
      */
    public static Date fastToDate(String time, String format) {
        if(time==null || "".equals(time.trim())) {
            return null;
        }
        Date date=null;
        if(Constants.TIMESATMP.equals(format.toUpperCase())){
            date = new Date();
        }else{
            DateFormat forMat =new SimpleDateFormat(format);
            try {
                date=forMat.parse(time);
            } catch (ParseException e) {
                logger.error("时间转换出错"+time+format);
            }
        }
        return date;
    }


     /**
      * @param  format:转换的日期格式
      * @return date:返回日期格式
      * @description 单独用于处理被更新表中的最后更新日期 eg.last_udt_tm 将当前系统时间按指定格式插入last_udt_tm中
      */
    public static Date fastToDateForUpdateTmCol(String format){
        Date date = null;
        if(Constants.TIMESATMP.equals(format.toUpperCase())
            || Constants.DATE.equals(format.toUpperCase())){//日期不需要进行格式转换
            date = new Date();
        }else{//日期需要进行格式转换
            DateFormat forMat =new SimpleDateFormat(format);
            try {
                date = forMat.parse(forMat.format(format));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;

    }

    //后续要用此方法需要重写
    public static Date fastToDate(Date time,String format) {
        Date date=null;
        if(Constants.TIMESATMP.equals(format.toUpperCase())){
            date = new Timestamp(new Date().getTime());
        }else{
            DateFormat forMat =new SimpleDateFormat(format);
            try {
                String string = forMat.format(time);
                date = forMat.parse(string);
            } catch (ParseException e) {
                logger.error("时间转换出错"+time+format);
            }
        }

        return date;
    }

     /**
      * @param time1:开始时间的毫秒数，time2:结束时间的毫秒数
      * @return 时间差的描述说明 eg.完成时间为 8小时26分钟23秒
      * @description 用于计算job完成时间
      */
    public static String calculateTimeMinus(Long time1,Long time2){
        Long minusTime = time2 - time1;
        //计算小时数
        Long hour = minusTime / (1000 * 60 * 60);
        //计算分钟数
        Long midMinute = minusTime % (1000 * 60 * 60) ;
        Long minute = midMinute / (1000 * 60);
        //计算秒数
        Long midSecond = midMinute % (1000 * 60);
        Long second = midSecond / 1000;
        return String.valueOf(hour)+"小时"+String.valueOf(minute)+"分钟"+String.valueOf(second)+"秒";
    }

    /**
     * 返回job参数
     * @param parameter
     * @return JobParameters
     */
    public static JobParameters createJobParams(String parameter) {
        return new JobParametersBuilder().addString("frequency", parameter).toJobParameters();
    }


    /**
     * 获取本地ip
     */
    public static String getlocalIp()
    {
        String localIp = "127.0.0.1";
        try {
            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            while(allNetInterfaces.hasMoreElements())
            {
                NetworkInterface networkInterface = allNetInterfaces.nextElement();
                if (networkInterface.isLoopback() || networkInterface.isVirtual() || !networkInterface.isUp())
                {
                    continue;
                }else
                {
                    Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
                    while (addresses.hasMoreElements())
                    {
                        ip = addresses.nextElement();
                        if (ip != null && ip instanceof Inet4Address)
                        {
                            return ip.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            logger.error("获取本地IP失败！",e);
        }

        return localIp;
    }
}
