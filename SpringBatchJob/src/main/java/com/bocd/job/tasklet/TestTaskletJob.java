package com.bocd.job.tasklet;

import com.bocd.job.base.AbstractBatchJob;
import com.bocd.job.base.GlobalDataDetectionUtil;
import com.bocd.job.listener.SingleJobListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * http://localhost:8088/springbatch/testTaskletJob/single?dataDate=20240115
 */
@Component
public class TestTaskletJob extends AbstractBatchJob {


    @Override
    public Job getJob(Map<String, Object> params, GlobalDataDetectionUtil initObj) throws Exception {
        JobExecutionListener listener = JobListenerFactoryBean.getListener(new SingleJobListener(initObj.getJobName(), initObj));

        Tasklet tasklet = (contribution, chunkContext) -> {
            for(int i = 0;i < 100;i++){
                logger.info("{}执行中{}...",initObj.getJobName(),i);
                Thread.sleep(1);
            }
            logger.info("{}执行完成.",initObj.getJobName());
            return RepeatStatus.FINISHED;
        };

        Step taskletStep = stepBuilderFactory.get(initObj.getJobName() + "_Step")
                .tasklet(tasklet)
                .build();
        if(params.containsKey("reRun") && "true".equals(params.get("reRun").toString())){
            return jobBuilderFactory.get(initObj.getJobName()).start(taskletStep)
                    .incrementer(new RunIdIncrementer())
                    .listener(listener).build();
        }else{
            return jobBuilderFactory.get(initObj.getJobName()).start(taskletStep)
                    .listener(listener).build();
        }

    }
}
