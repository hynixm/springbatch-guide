package com.bocd.job;

import com.bocd.entity.dto.FmCcyRateTemp;
import com.bocd.job.base.AbstractBatchJob;
import com.bocd.job.base.GlobalDataDetectionUtil;
import com.bocd.job.listener.SingleJobListener;
import com.bocd.mapper.second.dao.BaseSqlDao;
import com.bocd.mapper.second.dao.FmCcyRateTempMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.SimpleJobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * http://localhost:8088/springbatch/impFmCcyRateJob/run?dataDate=20240116&filename=/home/oracle/20240318/sym_fm_ccy_rate.dat
 */
@Component
public class ImpFmCcyRateJob extends AbstractBatchJob {

    @Autowired
    private BaseSqlDao baseSqlDao;

    @Autowired
    @Qualifier("secondSqlSessionFactory")
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    @Qualifier("stepTaskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;


    private Step getStep1(String jobName){
        Tasklet tasklet = (contribution, chunkContext) -> {
            logger.info("{}:fm_ccy_rate_temp 表数据加载前清空",jobName);
            baseSqlDao.truncateTab("fm_ccy_rate_temp");
            return RepeatStatus.FINISHED; //返回 FINISHED 表示该tasklet已经处理完成（无论是否成功）,可以继续下一段处理
        };
        Step taskletStep = stepBuilderFactory.get(jobName + "_step1").tasklet(tasklet).build();
        return taskletStep;
    }


    private FlatFileItemReader<FmCcyRateTemp> fileItemReader(Map<String, Object> params){
        FileSystemResource filename = new FileSystemResource(params.get("filename").toString());
        FlatFileItemReaderBuilder<FmCcyRateTemp> readerBuilder = new FlatFileItemReaderBuilder<>();
        FlatFileItemReader<FmCcyRateTemp> reader = readerBuilder
                //.name("DelimitedJob-reader") // 用于持久化到executionContex的唯一键,saveState设置为false就可以不用设置
                .saveState(false) //用于指示在处理每个块后，应该保存itemReader状态，已达到重启目的，如果多线程使用应该设置为false。这里重新执行希望重新从头加载数据，所以设置false
                //.linesToSkip(1) //跳过第1行
                .strict(true) //严格模式下，找不到资源将抛出异常
                .delimited() //默认分隔符 ,
                .delimiter("|") //指定分隔符
                //.quoteCharacter('#') // # 表示但引号，表示文件字段使用单引号括起来做字符串解析
                .names(new String[]{"ccy","branch","rateType","effectiveDate","lastChangeDate","quoteType","ccyRate","buyRate",
                        "sellRate","centralBankRate","buySpread","sellSpread","notesBuyRate","notesSellRate"
                })
                .targetType(FmCcyRateTemp.class)
                .resource(filename)
                .encoding("UTF-8")
                .build();
        return reader;
    }

    private ItemWriter<FmCcyRateTemp> fileItemWrite(){
        SqlSessionTemplate sqlSessionTemplate  = new SqlSessionTemplate(this.sqlSessionFactory, ExecutorType.BATCH);
        FmCcyRateTempMapper fmCcyRateTempMapper = sqlSessionTemplate.getMapper(FmCcyRateTempMapper.class);
        ItemWriter<FmCcyRateTemp> writer = new ItemWriter<FmCcyRateTemp>(){
            @Override
            public void write(List<? extends FmCcyRateTemp> items) throws Exception {
                for(FmCcyRateTemp fmCcyRateTemp : items){
                    fmCcyRateTempMapper.insert(fmCcyRateTemp);
                }
            }
        };
        return writer;
    }

    private Step getStep2(Map<String, Object> params,String jobName) throws Exception {
        return this.stepBuilderFactory.get(jobName + "_step2")
                .<FmCcyRateTemp, FmCcyRateTemp>chunk(5000)
                //查询 营销系统提供的 营销任务范围表数据
                .reader(fileItemReader(params))
                //对数据进行操作
                .writer(fileItemWrite())
                .faultTolerant()
                .taskExecutor(taskExecutor)
                .throttleLimit(3)
                .build();
    }

    @Override
    public Job getJob(Map<String, Object> params, GlobalDataDetectionUtil initObj) throws Exception {
        JobExecutionListener listener = JobListenerFactoryBean.getListener(new SingleJobListener(initObj.getJobName(), initObj));
        SimpleJobBuilder simpleJobBuilder =  jobBuilderFactory.get(initObj.getJobName()).start(getStep1(initObj.getJobName())).next(getStep2(params,initObj.getJobName())).listener(listener);
        if(params.containsKey("reRun") && "true".equals(params.get("reRun").toString())){
            return simpleJobBuilder.incrementer(new RunIdIncrementer()).build();
        }else{
            return simpleJobBuilder.build();
        }
    }
}
