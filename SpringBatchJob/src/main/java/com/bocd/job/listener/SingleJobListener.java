package com.bocd.job.listener;


import com.bocd.utils.CommonUtils;
import com.bocd.job.base.GlobalDataDetectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

import java.util.HashMap;
import java.util.Map;

public class SingleJobListener {
    private static Logger logger = LoggerFactory.getLogger(SingleJobListener.class);

    private GlobalDataDetectionUtil initObj;
    private String jobName;
    private Long beginTime;
    private Long endTime;

    public SingleJobListener(String jobName, GlobalDataDetectionUtil initObj) {
        this.jobName = jobName;
        this.initObj = initObj;
    }

    @BeforeJob
    public void beforJob(JobExecution jobExecution){
        Map<String, BatchStatus> map = new HashMap<>();
        map.put(jobName,jobExecution.getStatus());
        // 就简单设置job状态 （自己管理的状态：GlobalDataDetectionUtil.resultT）
        initObj.setResultT(map);
        beginTime = jobExecution.getStartTime().getTime();

        logger.info("SingleJobListener监听到即将开始执行job: "+ jobName);
    }

    @AfterJob
    public void afterJob(JobExecution jobExecution){
        // 设置job状态（自己管理的状态：GlobalDataDetectionUtil.resultT），并通知其他需要获取该job状态的线程可以继续执行
        initObj.setResultStatus(jobExecution.getStatus());
        endTime = jobExecution.getEndTime().getTime();
        logger.info(">>>>>>>>> 名称为："+jobName+"的job耗时："+ CommonUtils.calculateTimeMinus(beginTime,endTime));
    }
}
