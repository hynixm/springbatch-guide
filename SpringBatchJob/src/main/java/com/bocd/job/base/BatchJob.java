package com.bocd.job.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;

import java.util.Map;

/**
 * 批量任务工厂类
 */

public interface BatchJob {
    Logger logger = LoggerFactory.getLogger(BatchJob.class);

    Job getJob(Map<String, Object> params, GlobalDataDetectionUtil initObj) throws Exception;
}