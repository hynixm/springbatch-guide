package com.bocd.job.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局数据检测工具类
 * 1.用于检测job的执行状态，job执行完后会将job的执行状态存入 resultT eg. map.put("jobName","failed");
 * 说明：
 *   1.0 采用 while(true)循环方式获取job执行结果，弊端在job执行完之前一直占用cpu，消耗cpu资源，已弃用。
 *   2.0 采用线程通信的方式，可以减少cpu的消耗，只需job执行完后发送通知即可，前提是被通知线程必须先wait，通知线程再唤醒，不然有可能被通知线程永远无法唤醒。
 *   2.1 创建初始对象时，保证每个job创建的对象为单例模式
 *
 * @author Qh
 * @version 2.1
 * @date 2020/12/10
 */
public class GlobalDataDetectionUtil {

    private static Logger logger = LoggerFactory.getLogger(GlobalDataDetectionUtil.class);

    private static Map<String,GlobalDataDetectionUtil> initGlobalDataDetection = new HashMap<>();

    private String jobName;

    /** 存放job执行状态 */
    private Map<String, BatchStatus> resultT ;

    public GlobalDataDetectionUtil(Map<String, BatchStatus> resultT) {
        this.resultT = resultT;
    }

    /**
     * 根据job名称获取job最终执行结果
     *
     * @param jobName job名称
     * @return true:job正常执行结束,false:执行失败（执行异常或手动停止等）
     */
    public synchronized boolean getResultStatus(String jobName){
        boolean resultStatus = false;
        try {
            logger.info("线程：{}, 名称为【{}】的job开始等待通知",Thread.currentThread().getName(),jobName);
            wait();
            BatchStatus batchStatus = this.getResultT().get(jobName);
            logger.info("线程：{} , 名称为【{}】的job收到通知,执行结果为【{}】",Thread.currentThread().getName(),jobName,batchStatus);
            if(batchStatus == BatchStatus.COMPLETED ){
                resultStatus = true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        return resultStatus;
    }

    /**
     * job执行完后设置job状态
     *
     * @param status
     */
    public synchronized void setResultStatus(BatchStatus status){
        Map<String,BatchStatus> map = new HashMap<>();
        map.put(this.getJobName(),status);
        this.setResultT(map);
        notifyAll();
        logger.info("【{}】的job执行完成,结果为【{}】，开始发送通知",this.getJobName(),status);
    }

    /**
     * 根据job名称封装初始GlobalDataDetectionUtil对象
     *
     * @param jobName
     * @return GlobalDataDetectionUtil对象
     */
    public static GlobalDataDetectionUtil initGlobalDataDetectionObj(String jobName){
        if(initGlobalDataDetection.get(jobName) == null){
            Map<String,BatchStatus> initMap = new HashMap<>();
            initMap.put(jobName,null);
            GlobalDataDetectionUtil initObj = new GlobalDataDetectionUtil(initMap);
            initObj.setJobName(jobName);
            initGlobalDataDetection.put(jobName,initObj);
        }
        return initGlobalDataDetection.get(jobName);
    }


    public Map<String, BatchStatus> getResultT() {
        return resultT;
    }

    public void setResultT(Map<String, BatchStatus> resultT) {
        this.resultT = resultT;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
}
