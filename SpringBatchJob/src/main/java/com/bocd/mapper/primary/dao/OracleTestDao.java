package com.bocd.mapper.primary.dao;

import java.util.List;

public interface OracleTestDao {
    List<String> mapperTest();

}
