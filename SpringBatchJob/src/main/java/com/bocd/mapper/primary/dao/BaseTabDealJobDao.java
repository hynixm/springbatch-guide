package com.bocd.mapper.primary.dao;

import java.util.List;

public interface BaseTabDealJobDao {
    List<String> mapperTest();
    int insertZhdzGgjgm();
    int mergeUsrOrgbank();
    int insertUsrOrg1();
    int insertUsrOrg2();
    int insertUsrOrg3();
    int insertUsrOrg4();
    int insertSorgan();
    int insertZhdzOrgStruct();
    int insertZhdzOrgnArea();
    int insertZhdzOrgn();
}
