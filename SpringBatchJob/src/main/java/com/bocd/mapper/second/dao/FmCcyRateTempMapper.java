package com.bocd.mapper.second.dao;

import com.bocd.entity.dto.FmCcyRateTemp;

public interface FmCcyRateTempMapper {
    int insert(FmCcyRateTemp record);

    int insertSelective(FmCcyRateTemp record);
}