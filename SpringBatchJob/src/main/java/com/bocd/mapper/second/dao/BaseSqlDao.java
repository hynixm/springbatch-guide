package com.bocd.mapper.second.dao;

import java.util.List;
import java.util.Map;

/**
 * 基础公共sql处理
 */
public interface BaseSqlDao {
    List<String> mapperTest();
    void truncateTab(String tabName);
}
