package com.bocd.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置类
 * @author Qh
 * @version 1.0
 * @date 2020/12/2
 */
@Configuration
@EnableAsync
public class ThreadPoolConfig {
    private static Logger logger = LoggerFactory.getLogger(ThreadPoolConfig.class);
    
    //job配置
     
    @Value("${spring-batch.job.thread-pool.core-pool-size}")
    private int jobCorePoolSize;

    @Value("${spring-batch.job.thread-pool.max-pool-size}")
    private int jobMaxPoolSize;

    @Value("${spring-batch.job.thread-pool.keep-alive-time}")
    private int jobKeepAliveTime;

    @Value("${spring-batch.job.thread-pool.queue-capacity}")
    private int jobQueueCapacity;

    @Value("${spring-batch.job.thread-pool.thread-name-prefix}")
    private String jobThreadNamePrefix;

    
    //step配置
     
    @Value("${spring-batch.step.thread-pool.core-pool-size}")
    private int stepCorePoolSize;

    @Value("${spring-batch.step.thread-pool.max-pool-size}")
    private int stepMaxPoolSize;

    @Value("${spring-batch.step.thread-pool.keep-alive-time}")
    private int stepKeepAliveTime;

    @Value("${spring-batch.step.thread-pool.queue-capacity}")
    private int stepQueueCapacity;

    @Value("${spring-batch.step.thread-pool.thread-name-prefix}")
    private String stepThreadNamePrefix;

    @Bean
    public ThreadPoolTaskExecutor jobTaskExecutor(){
        logger.info(">>>>>>> jobTaskExecutor线程池初始化.");
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(jobCorePoolSize);
		taskExecutor.setMaxPoolSize(jobMaxPoolSize);
		taskExecutor.setKeepAliveSeconds(jobKeepAliveTime);
		taskExecutor.setQueueCapacity(jobQueueCapacity);
		taskExecutor.setThreadNamePrefix(jobThreadNamePrefix);//线程名称前缀默认为job的名称 eg.job-thread-
		taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
		taskExecutor.initialize();
		return taskExecutor;
    }

    @Bean
    public ThreadPoolTaskExecutor stepTaskExecutor(){
        logger.info(">>>>>>> stepTaskExecutor线程池初始化.");
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(stepCorePoolSize);
        taskExecutor.setMaxPoolSize(stepMaxPoolSize);
        taskExecutor.setKeepAliveSeconds(stepKeepAliveTime);
        taskExecutor.setQueueCapacity(stepQueueCapacity);
        taskExecutor.setThreadNamePrefix(stepThreadNamePrefix);//线程名称前缀默认为step的名称 eg.step-thread-
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        taskExecutor.initialize();
        return taskExecutor;
    }
}