package com.bocd.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
public class SimpleAsyncTaskExecutorCongfig {
	private static Logger logger = LoggerFactory.getLogger(SimpleAsyncTaskExecutorCongfig.class);
	
	@Value("${spring-batch.job.job-concurrency-limit}")
    private int jobConcurrencyLimit;

	@Bean
	public SimpleAsyncTaskExecutor jobSimpleAsyncTaskExecutor(){
		SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
		simpleAsyncTaskExecutor.setConcurrencyLimit(jobConcurrencyLimit);
		return simpleAsyncTaskExecutor;
	}


}
