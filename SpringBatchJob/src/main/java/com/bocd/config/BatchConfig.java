package com.bocd.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
	private static Logger logger = LoggerFactory.getLogger(BatchConfig.class);

	@Autowired
	private JobRepository jobRepository;
	
	@Value("${spring-batch.job.executor}")
	private String taskExecutorName;

	@Autowired
	@Qualifier("jobTaskExecutor")
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Autowired
	@Qualifier("jobSimpleAsyncTaskExecutor")
	private SimpleAsyncTaskExecutor jobSimpleAsyncTaskExecutor;
	
	@Bean
	public JobLauncher myJobLauncher() throws Exception {
		//根据application.yml的配置选择job的TaskExecutor
		logger.info("application.yml中配置的executor为[{}],开始初始化JobLauncher",new Object[]{taskExecutorName});
		TaskExecutor taskExecutor;
		switch (taskExecutorName) {
		case "SimpleAsyncTaskExecutor":
			taskExecutor = jobSimpleAsyncTaskExecutor;
			break;
		case "ThreadPoolTaskExecutor":
			taskExecutor = threadPoolTaskExecutor;
			break;
		default:
			taskExecutor = jobSimpleAsyncTaskExecutor;
			break;
		}
		
		
		SimpleJobLauncher jobLauncher =new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository);
		jobLauncher.setTaskExecutor(taskExecutor);	
		jobLauncher.afterPropertiesSet();
		return jobLauncher;
		
	}
}
