package com.bocd.config.datasource;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisMapper扫描配置
 *
 * @author jiangke.zh
 * @version 1.0
 * @since 2021/2/24 18:11
 */
@Configuration
@AutoConfigureAfter({PrimaryMybatisConfig.class, SecondMybatisConfig.class})
public class MybatisMapperScannerConfig {

    @Bean
    public MapperScannerConfigurer primaryMapperScannerConfig() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("primarySqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.bocd.mapper.primary");
        return mapperScannerConfigurer;
    }

    @Bean
    public MapperScannerConfigurer listMapperScannerConfig() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("secondSqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.bocd.mapper.second");
        return mapperScannerConfigurer;
    }

}
