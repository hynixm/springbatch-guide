package com.bocd.config.datasource;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * List数据源Mybatis配置
 *
 * @author jiangke.zh
 * @version 1.0
 * @since 2021/12/24 19:50
 */

@Configuration
@EnableTransactionManagement
public class SecondMybatisConfig extends BaseMybatisConfig {

    @Value("${second-data-source.type}")
    private String type;

    @Bean(name = "secondDataSource")
    @ConfigurationProperties(prefix = "second-data-source")
    @SuppressWarnings("unchecked")
    public DataSource secondDataSource() throws ClassNotFoundException {
        return DataSourceBuilder.create()
                .type((Class<? extends DataSource>) Class.forName(type))
                .build();
    }

    @Value("${second-mybatis.mapper-locations}")
    private String mapperLocation;

    @Value("${second-mybatis.type-aliases-package}")
    private String typeAliasesPackage;

    @Bean(name = "secondSqlSessionFactory")
    public SqlSessionFactory secondSqlSessionFactoryBean() throws Exception {
        MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
        bean.setDataSource(secondDataSource());
        bean.setGlobalConfig(globalConfig());
        bean.setConfiguration(mybatisConfiguration());
        bean.setTypeAliasesPackage(typeAliasesPackage);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        bean.setMapperLocations(resolver.getResources(mapperLocation));
        return bean.getObject();
    }

    @Bean
    public SqlSessionTemplate secondSqlSessionTemplate(@Qualifier("secondSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    @Bean(name = "secondDataTransaction")
    public DataSourceTransactionManager secondDataSourceTransactionManager(@Qualifier("secondDataSource") DataSource dataSource) throws ClassNotFoundException {
        return new DataSourceTransactionManager(dataSource);
    }

    private MybatisConfiguration mybatisConfiguration(){
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setGlobalConfig(globalConfig());
        mybatisConfiguration.setMapUnderscoreToCamelCase(true);
        mybatisConfiguration.setCacheEnabled(false);
        return mybatisConfiguration;
    }

}
