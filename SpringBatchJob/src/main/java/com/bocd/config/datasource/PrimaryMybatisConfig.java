package com.bocd.config.datasource;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * primary数据源Mybatis配置
 *
 * @author jiangke.zh
 * @version 1.0
 * @since 2021/2/24 18:12
 */
@Configuration
@EnableTransactionManagement
public class PrimaryMybatisConfig extends BaseMybatisConfig {

    @Value("${primary-data-source.type}")
    private String type;

    @Bean(name = "primaryDataSource")
    @ConfigurationProperties(prefix = "primary-data-source")
    @SuppressWarnings("unchecked")
    @Primary
    public DataSource primaryDataSource() throws ClassNotFoundException {
        return DataSourceBuilder.create()
                .type((Class<? extends DataSource>) Class.forName(type))
                .build();
    }

    @Value("${primary-mybatis.mapper-locations}")
    private String mapperLocation;

    @Value("${primary-mybatis.type-aliases-package}")
    private String typeAliasesPackage;

    @Bean(name = "primarySqlSessionFactory")
    @Primary
    public SqlSessionFactory primarySqlSessionFactoryBean() throws Exception {
        MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
        bean.setDataSource(primaryDataSource());
        bean.setGlobalConfig(globalConfig());
        bean.setConfiguration(mybatisConfiguration());
        bean.setTypeAliasesPackage(typeAliasesPackage);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        bean.setMapperLocations(resolver.getResources(mapperLocation));
        return bean.getObject();
    }

    @Bean
    @Primary
    public SqlSessionTemplate primarySqlSessionTemplate(@Qualifier("primarySqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    @Bean(name = "primaryDataTransaction")
    @Primary
    public DataSourceTransactionManager primaryDataSourceTransactionManager(@Qualifier("primaryDataSource") DataSource dataSource) throws ClassNotFoundException {
        return new DataSourceTransactionManager(dataSource);
    }

    private MybatisConfiguration mybatisConfiguration() {
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setGlobalConfig(globalConfig());
        mybatisConfiguration.setMapUnderscoreToCamelCase(true);
        mybatisConfiguration.setCacheEnabled(false);
        return mybatisConfiguration;
    }
}
