package com.bocd.config.datasource;

import com.baomidou.mybatisplus.core.config.GlobalConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 数据源Mybatis基础配置
 *
 * @author jiangke.zh
 * @version 1.0
 * @since 2021/21/24 19:56
 */

//@Configuration
public class BaseMybatisConfig {

    @ConfigurationProperties(prefix = "mybatis-plus.global-config")
    GlobalConfig globalConfig(){
        return new GlobalConfig();
    }

}