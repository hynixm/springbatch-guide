package com.example.Chapter04.bean;

import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JobLaunchRequestIdentifying {
    private String JobName;

    private Map<String,String> jobParameters;

    public String getJobName() {
        return JobName;
    }

    public void setJobName(String jobName) {
        JobName = jobName;
    }

    //所有的job参数，封装成识别性参数
    public JobParameters getJobParameters() {

        Map<String, JobParameter> parameterMap = new HashMap<>();
        Set<String> keys = this.jobParameters.keySet();
        for(String param : keys){
            parameterMap.put(param, new JobParameter(this.jobParameters.get(param), true)); //识别性参数（每次输入的这个值不同，就可以执行）
        }
        JobParameters jobParameters = new JobParameters(parameterMap);

        return jobParameters;
    }
}
