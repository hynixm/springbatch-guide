package com.example.Chapter04.jobs;

import com.example.Chapter04.batch.RandomDecider;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 步骤流演示，自定义一个逻辑步骤，设定步骤结果状态
 */
@Component
public class ConditionalJobDecider extends AbstractBatchJob{

    @Autowired
    private RandomDecider decider;//自定义逻辑步骤（状态随机 COMPLETED、FAILED）

    public Tasklet passTasklet() {
        return (contribution, chunkContext) -> {
			return RepeatStatus.FINISHED;
            //throw new RuntimeException("Causing a failure");
        };
    }

    public Tasklet successTasklet() {
        return (contribution, context) -> {
            System.out.println("Success!");
            return RepeatStatus.FINISHED;
        };
    }

    public Tasklet failTasklet() {
        return (contribution, context) -> {
            System.out.println("Failure!");
            return RepeatStatus.FINISHED;
        };
    }


    public Step firstStep() {
        return this.stepBuilderFactory.get("firstStep")
                .tasklet(passTasklet())
                .build();
    }

    public Step successStep() {
        return this.stepBuilderFactory.get("successStep")
                .tasklet(successTasklet())
                .build();
    }

    public Step failureStep() {
        return this.stepBuilderFactory.get("failureStep")
                .tasklet(failTasklet())
                .build();
    }

    /**
     * step逻辑串联，if/else
     * @return
     */
	public Job getJob() {
		return this.jobBuilderFactory.get("conditionalJobDecider")
				.start(firstStep())
                .next(decider)
                .from(decider).on("FAILED").to(failureStep())
				// 如果执行firstStep，成功，上一个条件拦不住，就走这里，接着执行successStep
				.from(decider).on("*").to(successStep())
				.end()
				.build();
	}


}
