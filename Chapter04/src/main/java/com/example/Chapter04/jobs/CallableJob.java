/**
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter04.jobs;

import java.util.concurrent.Callable;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.CallableTaskletAdapter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Tasklet模式 CallableTaskletAdapter 的示例
 * 执行逻辑在一个新的线程中
 */
@Component
public class CallableJob extends AbstractBatchJob{

	/**
	 * 封装job
	 * @return
	 */
	public Job getJob() {
		return this.jobBuilderFactory.get("callableJob")
				.start(callableStep())
				.build();
	}

	/**
	 * 封装step
	 * @return
	 */
	public Step callableStep() {
		return this.stepBuilderFactory.get("callableStep")
				.tasklet(tasklet())
				.build();
	}

	/**
	 *
	 * @return 返回一个实现 Callable接口的 回调线程对象
	 */
	public Callable<RepeatStatus> callableObject() {
		return () -> {
			System.out.println("This was executed in another thread[" + Thread.currentThread().getName() +"]");

			return RepeatStatus.FINISHED;// 执行逻辑虽然在一个新的线程，但是step还是要等待Callable返回有效对象后，step才能认定执行完成。
		};
	}

	/**
	 * step的具体业务：基于tasklet模型
	 * @return
	 */
	public CallableTaskletAdapter tasklet() {
		System.out.println("This step thread[" + Thread.currentThread().getName() +"]");
		//使用 CallableTaskletAdapter ，可以让执行逻辑在一个新的线程中执行，不与step同一个线程
		CallableTaskletAdapter callableTaskletAdapter = new CallableTaskletAdapter();
		callableTaskletAdapter.setCallable(callableObject());

		return callableTaskletAdapter;
	}

}
