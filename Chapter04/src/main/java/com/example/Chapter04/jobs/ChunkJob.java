/**
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter04.jobs;

import com.example.Chapter04.batch.LoggingStepStartStopListener;
import com.example.Chapter04.batch.RandomChunkSizePolicy;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.batch.repeat.CompletionPolicy;
import org.springframework.batch.repeat.policy.CompositeCompletionPolicy;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.batch.repeat.policy.TimeoutTerminationPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import com.example.Chapter04.batch.RandomChunkSizePolicy;

/**
 * 基于chunk的作业
 * chunk大小控制：
 * 	1. TimeoutTerminationPolicy 设置chunk超时时间，到达时间chunk整个块提交
 * 	2. SimpleCompletionPolicy 统计处理过条目的数量，设置阈值，chunk处理达到这个量就结束，整体提交
 * 	3. 自定义CompletionPolicy的实现类，这里演示了一个20以内随机分块的工具类 RandomChunkSizePolicy
 */
@Component
public class ChunkJob extends AbstractBatchJob{

	@Autowired
	private RandomChunkSizePolicy randomChunkSizePolicy; //自定义实现控制chunk大小

	@Autowired
	private JobExplorer explorer;

	public Job getJob() {
		return this.jobBuilderFactory.get("chunkBasedJob")
				.start(chunkStep())
                .incrementer(new RunIdIncrementer())
				.build();
	}

	public Step chunkStep() {
		return this.stepBuilderFactory.get("chunkStep")
				//.<String, String>chunk(1000)
				.<String, String>chunk(randomChunkSizePolicy)//自定义实现控制chunk大小
                //.<String,String>chunk(completionPolicy())
				.reader(itemReader())
				.writer(itemWriter())
				.listener(new LoggingStepStartStopListener())
				.build();
	}

	// list 读取器，一次读取100000条返回，然后chunk里再分块writer
	public ListItemReader<String> itemReader() {
		List<String> items = new ArrayList<>(100000);

		for (int i = 0; i < 100000; i++) {
			items.add(UUID.randomUUID().toString());
		}

		return new ListItemReader<>(items);
	}

	public ItemWriter<String> itemWriter() {
		return items -> {
			for (String item : items) {
				System.out.println(">> current item = " + item);
			}
		};
	}


    /**
     * chunk完成代理类
     * 配置chunk处理完成的多个条件（这些所有的条件决定一个chunk的大小，区别与固定大小的chunk）
     * @return
     */
    public CompletionPolicy completionPolicy() {
        CompositeCompletionPolicy policy = new CompositeCompletionPolicy();

        policy.setPolicies(
                new CompletionPolicy[]{
                        //TimeoutTerminationPolicy 设置chunk超时时间，到达时间chunk整个块提交
                    new TimeoutTerminationPolicy(3),
                        //SimpleCompletionPolicy 统计处理过条目的数量，设置阈值，chunk处理达到这个量就结束，整体提交
                    new SimpleCompletionPolicy(1000)});

        return policy;
    }

}
