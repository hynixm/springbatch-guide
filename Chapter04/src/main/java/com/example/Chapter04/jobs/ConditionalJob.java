/**
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter04.jobs;

import com.example.Chapter04.batch.RandomDecider;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * 步骤流演示
 */
@Component
public class ConditionalJob extends AbstractBatchJob{


	public Tasklet passTasklet() {
		return (contribution, chunkContext) -> {
			throw new RuntimeException("Causing a failure");
		};
	}

	public Tasklet successTasklet() {
		return (contribution, context) -> {
			System.out.println("Success!");
			return RepeatStatus.FINISHED;
		};
	}

	public Tasklet failTasklet() {
		return (contribution, context) -> {
			System.out.println("Failure!");
			return RepeatStatus.FINISHED;
		};
	}


	public Step firstStep() {
		return this.stepBuilderFactory.get("firstStep")
				.tasklet(passTasklet())
				.build();
	}

	public Step successStep() {
		return this.stepBuilderFactory.get("successStep")
				.tasklet(successTasklet())
				.build();
	}

	public Step failureStep() {
		return this.stepBuilderFactory.get("failureStep")
				.tasklet(failTasklet())
				.build();
	}

	public JobExecutionDecider decider() {
		return new RandomDecider();
	}

	/**
	 * step逻辑串联，if/else
	 * @return
	 */
/*
	public Job getJob() {
		return this.jobBuilderFactory.get("conditionalJob")
				// 执行firstStep，失败，符合步骤状态FAILED，即执行failureStep
				// failureStep最后会返回 RepeatStatus.FINISHED，即作业完成，就不会再去走from(....
				.start(firstStep()).on("FAILED").to(failureStep())
				// 如果执行firstStep，成功，上一个条件拦不住，就走这里，接着执行successStep
				.from(firstStep()).on("*").to(successStep())
				.end()
				.build();
	}
*/

	//job执行结果状态 FAILED，可以相同参数重新执行
/*	public Job getJob() {
		return this.jobBuilderFactory.get("conditionalJob")
				// 执行firstStep，失败，此时jobInstance的状态是STOPPED；
				.start(firstStep())
				.build();
	}*/

	//job执行结果状态 FAILED，可以相同参数重新执行
	public Job getJob() {
		return this.jobBuilderFactory.get("conditionalJob")
				// 执行firstStep，失败，符合条件on("FAILED")，将作业最后状态置为 FAILED；
				.start(firstStep()).on("FAILED").fail()
				.from(firstStep()).on("*").to(successStep()).end()
				.build();
	}

	// 以完成状态结束作业
/*	public Job getJob() {
		return this.jobBuilderFactory.get("conditionalJob")
				// 执行firstStep，失败，符合条件on("FAILED")，将作业最后状态置为 COMPLETED
				.start(firstStep()).on("FAILED").end()
				.from(firstStep()).on("*").to(successStep()).end() //这一串为什么一定要，可以理解为if/else，必须要有一个else
				.build();
	}*/

/*
	// 作业停止重启，重设置的停止重启步骤开始
	public Job getJob() {
        return this.jobBuilderFactory.get("conditionalJob")
				// 执行firstStep，抛出异常，步骤的结果是FAILED，
				// 符合条件 on("FAILED").stopAndRestart...，此时作业停止，作业状态是STOPPED；
				// 第二次同样的参数执行（同一个jobInstance）,因为上一次的执行状态是STOPPED，所以从停止的地方重启（作业中也设置了stopAndRestart）
                .start(firstStep()).on("FAILED").stopAndRestart(successStep())
                .from(firstStep()).on("*").to(successStep())
                .end()
                .build();
    }*/
}
