package com.example.Chapter04.jobs;

import org.springframework.batch.core.Job;

public interface BatchJob {

    Job getJob();
}
