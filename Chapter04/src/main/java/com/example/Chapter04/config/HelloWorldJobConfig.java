package com.example.Chapter04.config;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class HelloWorldJobConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

   @Bean(name = "helloWorldJobStep1")
    public Step HelloWorldJobStep1() {
        return this.stepBuilderFactory.get("helloWorldJobStep1")
                .tasklet(helloWorldTasklet(null, null))
                .build();
    }

    @StepScope //spring延迟绑定功能，将作业参数注入到组件当中
    @Bean
    public Tasklet helloWorldTasklet(
            @Value("#{jobParameters['name']}") String name,
            @Value("#{jobParameters['fileName']}") String fileName) {

        return (contribution, chunkContext) -> {
            Map<String, Object> jobParameters = chunkContext.getStepContext().getJobParameters();
            System.out.println(String.format("Hello, %s!", name));
            System.out.println(String.format("fileName = %s", fileName));

            return RepeatStatus.FINISHED;
        };
    }
}
