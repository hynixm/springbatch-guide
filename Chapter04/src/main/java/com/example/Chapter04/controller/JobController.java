package com.example.Chapter04.controller;

import com.example.Chapter04.bean.JobLaunchRequest;
import com.example.Chapter04.bean.JobLaunchRequestIdentifying;
import com.example.Chapter04.jobs.BatchJob;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 执行作业 helloWorldJob 示例：
 * http://localhost:8080/job?jobName=helloWorldJob&currentBatch=20220224
 */

@RestController
@RequestMapping("/job")
public class JobController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;


    //接受固定参数，执行的job使用run.id（自增）做为识别性参数
    @RequestMapping(value = "/runid",method = RequestMethod.GET)
    public ExitStatus runJob(@RequestParam(value = "jobName", required = true) String jobName, @RequestParam(value = "currentBatch", required = false) String currentBatch) throws Exception {
        Job job = this.context.getBean(jobName, BatchJob.class).getJob();

        Properties properties = new Properties();
        properties.put("jobName",jobName);
        properties.put("currentBatch",currentBatch);
        JobParameters jobParameters = new JobParametersBuilder(properties).toJobParameters();

        /**
         * 这里的封装的参数 jobName、currentBatch 都是非识别性的，只有“getNextJobParameters(job)”作用封装的run.id是识别性的。
         * run.id自增原理就是查询作业上一次执行的run.id值，然后加1；jobExplorer就是对存储库做查询的组件
         *
         */
        JobParameters jobParameters1 = new JobParametersBuilder(jobParameters,this.jobExplorer).getNextJobParameters(job)
                .toJobParameters();

        return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }

    //接受固定参数，执行的job使用currentBatch 做为识别性参数，所以多次调用同一作业每次的currentBatch值需要不一样。
    @RequestMapping(value = "/current/batch",method = RequestMethod.GET)
    public ExitStatus runJob1(@RequestParam(value = "jobName", required = true) String jobName, @RequestParam(value = "currentBatch", required = false) String currentBatch) throws Exception {

        Job job = this.context.getBean(jobName, BatchJob.class).getJob();

        Map<String, JobParameter> parameterMap = new HashMap<>();
        parameterMap.put("jobName", new JobParameter(jobName, false)); //非识别性参数
        parameterMap.put("currentBatch", new JobParameter(currentBatch, true)); //识别性参数（每次输入的这个值不同，就可以执行）
        JobParameters jobParameters = new JobParameters(parameterMap);

        JobParameters jobParameters1 = new JobParametersBuilder(jobParameters,this.jobExplorer)
                //.getNextJobParameters(job) //自增量参数run.id
                .toJobParameters();

        return this.jobLauncher.run(job, jobParameters1).getExitStatus();
    }

    //灵活参数
    @RequestMapping(value = "/paramters",method = RequestMethod.POST)
    public ExitStatus runJobRequestBody(@RequestBody JobLaunchRequest request) throws Exception {
        Job job = this.context.getBean(request.getJobName(), BatchJob.class).getJob();

        JobParameters jobParameters =
                new JobParametersBuilder(request.getJobParameters(),
                        this.jobExplorer)
                        .getNextJobParameters(job) // job中封装了JobParametersIncrementer的实现类的实例化对象（使用incrementer方法），这里利用这个对象给job封装一个自动增量或者变化的识别性参数
                        .toJobParameters();

        return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }

    //jobName，直接执行
    @RequestMapping(value = "/name",method = RequestMethod.GET)
    public ExitStatus runJobForName(@RequestParam(value = "jobName", required = true) String jobName) throws Exception {
        Job job = this.context.getBean(jobName, BatchJob.class).getJob();

        Map<String, JobParameter> parameterMap = new HashMap<>();
        parameterMap.put("jobName", new JobParameter(jobName, false)); //非识别性参数
        JobParameters jobParameters = new JobParameters(parameterMap);


        JobParameters jobParameters1 = new JobParametersBuilder(jobParameters,this.jobExplorer)
                .getNextJobParameters(job)
                .toJobParameters();

        return this.jobLauncher.run(job, jobParameters1).getExitStatus();
    }

    //JobLaunchRequestIdentifying 将所有参数封装成识别性参数
    @RequestMapping(value = "/myparam",method = RequestMethod.POST)
    public ExitStatus runJobRequestBody1(@RequestBody JobLaunchRequestIdentifying request) throws Exception {
        Job job = this.context.getBean(request.getJobName(), BatchJob.class).getJob();

        return this.jobLauncher.run(job, request.getJobParameters()).getExitStatus();
    }

    //灵活参数
    @RequestMapping(value = "/noparam",method = RequestMethod.POST)
    public ExitStatus runJobRequestBody2(@RequestBody JobLaunchRequest request) throws Exception {
        Job job = this.context.getBean(request.getJobName(), BatchJob.class).getJob();
        return this.jobLauncher.run(job,  request.getJobParameters()).getExitStatus();
    }
}
