package com.example.Chapter04;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


import com.alibaba.fastjson.JSONObject;

public class JobRunTest {

    @Test
    public void helloWorldJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/job/runid";
        Map<String, String> params = new HashMap<String, String>();
        params.put("jobName", "helloWorldJob");
        params.put("currentBatch", "20220201");
        restTemplate.getForObject(url + "?jobName={jobName}&currentBatch={currentBatch}",String.class, params);
    }

    @Test
    public void helloWorldJob1(){
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/paramters";
        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);
        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        Properties properties = new Properties();
        properties.put("name","hynixm");
        properties.put("fileName","file.csv");
        map.put("jobName", "helloWorldJob");
        map.put("jobParameters", properties);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串；
        String body = entity.getBody();
        System.out.println(body);
    }

    @Test
    public void callableJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/job/current/batch";
        Map<String, String> params = new HashMap<String, String>();
        params.put("jobName", "callableJob");
        params.put("currentBatch", "20220204");
        restTemplate.getForObject(url + "?jobName={jobName}&currentBatch={currentBatch}",String.class, params);
    }

    @Test
    public void methodInvokingJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/paramters";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        Properties properties = new Properties();
        properties.put("message","this is a methodInvokingJob,now execute!");
        map.put("jobName", "methodInvokingJob");
        map.put("jobParameters", properties);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        String body = entity.getBody();
        System.out.println(body);
    }


    @Test
    public void systemCommandJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/paramters";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobName", "systemCommandJob");
        map.put("jobParameters", new Properties());

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }

    @Test
    public void chunkJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/job/name";
        Map<String, String> params = new HashMap<String, String>();
        params.put("jobName", "chunkJob");
        restTemplate.getForObject(url + "?jobName={jobName}",String.class, params);
    }


    @Test
    public void conditionalJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/myparam";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobName", "conditionalJob");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("job3","2");
        map.put("jobParameters",mapParams);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }

    @Test
    public void conditionalJobDeciderTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/myparam";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobName", "conditionalJobDecider");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("job","5");
        map.put("jobParameters",mapParams);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }

    @Test
    public void flowJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/myparam";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobName", "flowJob");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("job","2");
        map.put("jobParameters",mapParams);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }

    @Test
    public void jobJobTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/job/noparam";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobName", "jobJob");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("job","1");
        map.put("jobParameters",mapParams);

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }
}
