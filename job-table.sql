select * from springbatch.batch_job_instance where job_name='transactionJob1' order by  JOB_INSTANCE_ID desc

select * from springbatch.batch_job_execution
where JOB_INSTANCE_ID = (
    select max(JOB_INSTANCE_ID) from springbatch.batch_job_instance
    where job_name='transactionJob1')
order by JOB_EXECUTION_ID desc

select * from springbatch.batch_step_execution
where JOB_EXECUTION_ID=(
    select max(JOB_EXECUTION_ID) from springbatch.batch_job_execution
    where JOB_INSTANCE_ID = (
        select max(JOB_INSTANCE_ID) from springbatch.batch_job_instance
        where job_name='transactionJob1')
)

select * from springbatch.batch_job_execution_params
where JOB_EXECUTION_ID=(
    select max(JOB_EXECUTION_ID) from springbatch.batch_job_execution
    where JOB_INSTANCE_ID = (
        select max(JOB_INSTANCE_ID) from springbatch.batch_job_instance
        where job_name='transactionJob1')