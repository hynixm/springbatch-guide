package com.example.Chapter08.util;

import java.util.HashMap;

// 单列模式 job参数缓存 工具
public class CacheUtil extends HashMap<String,Object> {
    private static volatile CacheUtil instance;

    private CacheUtil() {
    }

    public static CacheUtil getInstance() {
        if (instance == null) {
            synchronized (CacheUtil.class) {
                if (instance == null) {
                    instance = new CacheUtil();
                }
            }
        }
        return instance;
    }

    /**
     * 添加缓存
     *
     * @param key
     * @param obj
     */
    public void addCacheData(String key, Object obj) {
        instance.put(key, obj);
    }

    /**
     * 取出缓存
     *
     * @param key
     * @return
     */
    public Object getCacheData(String key) {
        return instance.get(key);
    }

    /**
     * 清楚缓存
     *
     * @param key
     */
    public void removeCacheData(String key) {
        instance.remove(key);
    }

}
