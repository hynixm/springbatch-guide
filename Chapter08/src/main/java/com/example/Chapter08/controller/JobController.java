package com.example.Chapter08.controller;

import com.example.Chapter08.job.domain.BatchJob;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//@RestController 是@controller和@ResponseBody 的结合
//@ResponseBody 它的作用简短截说就是指该类中所有的API接口返回的数据，甭管你对应的方法返回Map或是其他Object，它会以Json字符串的形式返回给客户端
@RestController
@RequestMapping("/springbatch8")
public class JobController {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;


    /**
     * 灵活参数，名称映射
     * @RequestParam（）指定的参数可以是普通元素、数组、集合、对象等等
     */
    @RequestMapping(value = "/{jobName}/map/run")
    public ExitStatus runJobMapParamAutoAdd(@PathVariable("jobName") String jobName, @RequestParam Map<String,String> params) throws Exception {
        /*Map<String, JobParameter> parameterMap = new HashMap<>();
        for(String key : params.keySet()){
            parameterMap.put(key, new JobParameter(params.get(key), true));
        }
        JobParameters jobParameters = new JobParameters(parameterMap); // job参数
        */
        /*if(CacheUtil.getInstance().containsKey(jobName)){
            String info = "job[" + jobName + "]已经在执行，请稍后再执行";
            System.out.println(info);
            return info;
        }*/
        // 组件方法获取job
        Job job = this.context.getBean(jobName, BatchJob.class).getJob(params);//数据统一以getJob方法传入
        JobParameters jobParameters = null;
        if(params.containsKey("myid")){//
            // job参数,自定义识别性参数，用于对那些上次执行异常的job，再次执行时使用相同的参数，这样从断点的数据开始执行
            Map<String, JobParameter> parameterMap = new HashMap<>();
            parameterMap.put("myid", new JobParameter(params.get("myid"), true));
            jobParameters = new JobParameters(parameterMap);
        } else {
            // job参数，增加自增参数（作为job的识别性参数）
            jobParameters =
                    new JobParametersBuilder(new JobParameters(),
                            this.jobExplorer)
                            .getNextJobParameters(job)// job中封装了JobParametersIncrementer的实现类的实例化对象（使用incrementer方法），这里利用这个对象给job封装一个自动增量或者变化的识别性参数
                            .toJobParameters();
        }
        return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }
}
