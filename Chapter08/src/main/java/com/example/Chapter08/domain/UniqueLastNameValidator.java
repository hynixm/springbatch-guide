/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter08.domain;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamSupport;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Michael Minella
 * 实现 Validator 接口就能完成自定义校验的功能
 * 继承 ItemStreamSupport ，自己实现作业重新执行保证校验的延续和准确
 *
 * 重新执行，从上次异常的数据开始执行，这个是springbatch框架来搞定的（其实就是job存储库中存了上次执行提交了多少条数据），本代码不涉及
 *
 * 官方话语：ItemStreamSupport的两个方法，保存作业执行之间的状态
 */
public class UniqueLastNameValidator extends ItemStreamSupport implements Validator<Customer> {

	private Set<String> lastNames = new HashSet<>();

	// 校验，数据有重复的lastName就报异常
	@Override
	public void validate(Customer value) throws ValidationException {
		if(lastNames.contains(value.getLastName())) {
			throw new ValidationException("Duplicate last name was found: " + value.getLastName());
		}

		// 每次校验通过一条新的数据，就不lastName放到列表中（set自动去重）
		this.lastNames.add(value.getLastName());
	}

	//数据提交后，即每一个chunk调用一次
	//关键数据,经提交的未重复的所有lastNames,保存在job存储库中.(因为open中初始化上一次作业执行，所以也包括以前这个作业执行提交的数据)
	@Override
	public void update(ExecutionContext executionContext) {
		executionContext.put(getExecutionContextKey("lastNames"), this.lastNames);
	}

	// 初始化springbatch框架中process时执行，即执行作业时只调用一次
	// 每次重复执行，就把上一次执行后保存在job存储库中的关键数据拿出来，即已经提交的未重复的所有lastNames
	@Override
	public void open(ExecutionContext executionContext) {
		String lastNames = getExecutionContextKey("lastNames");

		if(executionContext.containsKey(lastNames)) {
			this.lastNames = (Set<String>) executionContext.get(lastNames);
		}
	}
}
