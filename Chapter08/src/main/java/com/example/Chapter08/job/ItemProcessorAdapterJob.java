package com.example.Chapter08.job;

import com.example.Chapter08.domain.Customer;
import com.example.Chapter08.job.domain.AbstractBatchJob;
import com.example.Chapter08.service.UpperCaseNameService;
import com.example.Chapter08.util.CacheUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.adapter.ItemProcessorAdapter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ItemProcessorAdapterJob extends AbstractBatchJob {
	

	public FlatFileItemReader<Customer> customerItemReader() {
		String inputFilePath = (String) CacheUtil.getInstance().get("ItemProcessorAdapterJob");
		FileSystemResource inputFile = new FileSystemResource(inputFilePath);

		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.delimited()
				.names(new String[] {"firstName",
						"middleInitial",
						"lastName",
						"address",
						"city",
						"state",
						"zip"})
				.targetType(Customer.class)
				.resource(inputFile)
				.build();
	}

	
	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}

	
	public ItemProcessorAdapter<Customer, Customer> itemProcessor(UpperCaseNameService service) {
		ItemProcessorAdapter<Customer, Customer> adapter = new ItemProcessorAdapter<>();

		adapter.setTargetObject(service);
		adapter.setTargetMethod("upperCase");

		return adapter;
	}

	
	public Step copyFileStep() {

		return this.stepBuilderFactory.get("ItemProcessorAdapterJob-copyFileStep")
				.<Customer, Customer>chunk(5)
				.reader(customerItemReader())
				.processor(itemProcessor(null))
				.writer(itemWriter())
				.build();
	}

	
	public Job getJob(Map<String, String> params) {

		return this.jobBuilderFactory.get("ItemProcessorAdapterJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())
				.build();
	}
}

