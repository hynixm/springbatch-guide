package com.example.Chapter08.job;

import com.example.Chapter08.domain.Customer;
import com.example.Chapter08.domain.UniqueLastNameValidator;
import com.example.Chapter08.job.domain.AbstractBatchJob;
import com.example.Chapter08.job.domain.JobListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * http://localhost:8080/springbatch8/validationJob/map/run?customerFile=Chapter08/input/customer.csv
 * http://localhost:8080/springbatch8/validationJob/map/run?myid=4&customerFile=Chapter08/input/customer.csv
 */
@Component
public class ValidationJob extends AbstractBatchJob {

	public FlatFileItemReader<Customer> customerItemReader(Map<String,String> param) {
		String inputFilePath = param.get("customerFile");
		FileSystemResource inputFile = new FileSystemResource(inputFilePath);

		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.delimited()
				.names(new String[] {"firstName",
						"middleInitial",
						"lastName",
						"address",
						"city",
						"state",
						"zip"})
				.targetType(Customer.class)
				.resource(inputFile)
				.build();
	}

	
	public ItemWriter<Customer> itemWriter() {

		//return (items) -> items.forEach(System.out::println);

		ItemWriter itemWriter = new ItemWriter() {
			@Override
			public void write(List items) throws Exception {
				//Thread.sleep(5000);
				items.forEach(System.out::println);
			}
		};
		return itemWriter;
	}


	public UniqueLastNameValidator validator() {
		UniqueLastNameValidator uniqueLastNameValidator = new UniqueLastNameValidator();
		uniqueLastNameValidator.setName("validator");
		return uniqueLastNameValidator;
	}

	
	public ValidatingItemProcessor<Customer> customerValidatingItemProcessor(UniqueLastNameValidator validator) {
		return new ValidatingItemProcessor<>(validator);
	}

/*	// springbatch框架提供的校验器 BeanValidatingItemProcessor
	// 具体校验在Customer属性的注解中
	public BeanValidatingItemProcessor<Customer> customerValidatingItemProcessor() {
		//return new BeanValidatingItemProcessor<Customer>();
		BeanValidatingItemProcessor<Customer> customerBeanValidatingItemProcessor = new BeanValidatingItemProcessor<>();
		try {
			customerBeanValidatingItemProcessor.afterPropertiesSet();//书中少了这一步，坑死了
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerBeanValidatingItemProcessor;
	}*/

	
	public Step copyFileStep(Map<String,String> param) {

		// 原本代码validator()有@Bean注解，在本文件代码中validator()就是获取那个bean
		// 改造后就不行了，必须用传参的方式保证是一个对象。不然再用validator()就是搞一个新的对象了
		UniqueLastNameValidator validator = validator();
		return this.stepBuilderFactory.get("validationJob-copyFileStep")
				.<Customer, Customer>chunk(5)
				.reader(customerItemReader(param))
				.processor(customerValidatingItemProcessor(validator))
				.writer(itemWriter())
				.stream(validator)//校验器中控制终端重新执行的逻辑，确保能够正确的跳过异常数据，正常接着后续数据执行job
				.build();
	}

	
	public Job getJob(Map<String, String> params) {//数据统一以getJob方法传入
		if(params.containsKey("myid")){ // 无自增参数
			return this.jobBuilderFactory.get("validationJob")
					.start(copyFileStep(params))
					.listener(JobListenerFactoryBean.getListener(new JobListener())) // job监听器
					.build();
		} else {
			return this.jobBuilderFactory.get("validationJob")
					.start(copyFileStep(params))
					.incrementer(new RunIdIncrementer())
					.listener(JobListenerFactoryBean.getListener(new JobListener())) // job监听器
					.build();
		}

	}

}

