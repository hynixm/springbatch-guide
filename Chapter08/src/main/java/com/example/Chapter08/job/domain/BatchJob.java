package com.example.Chapter08.job.domain;

import org.springframework.batch.core.Job;

import java.util.Map;

// 统一获取springbatch job 的接口方法
public interface BatchJob {

    Job getJob(Map<String, String> params);//数据统一以getJob方法传入
}
