package com.example.Chapter08.job;

import com.example.Chapter08.domain.Customer;
import com.example.Chapter08.domain.UniqueLastNameValidator;
import com.example.Chapter08.job.domain.AbstractBatchJob;
import com.example.Chapter08.service.UpperCaseNameService;
import com.example.Chapter08.util.CacheUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.adapter.ItemProcessorAdapter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.item.support.ScriptItemProcessor;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Component
public class CompositeItemProcessorJob extends AbstractBatchJob {


	@StepScope
	public FlatFileItemReader<Customer> customerItemReader() {
		String inputFilePath = (String) CacheUtil.getInstance().get("CompositeItemProcessorJob");
		FileSystemResource inputFile = new FileSystemResource(inputFilePath);

		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.delimited()
				.names(new String[] {"firstName",
						"middleInitial",
						"lastName",
						"address",
						"city",
						"state",
						"zip"})
				.targetType(Customer.class)
				.resource(inputFile)
				.build();
	}

	
	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}

	
	public UniqueLastNameValidator validator() {
		UniqueLastNameValidator uniqueLastNameValidator = new UniqueLastNameValidator();

		uniqueLastNameValidator.setName("validator");

		return uniqueLastNameValidator;
	}

	
	public ValidatingItemProcessor<Customer> customerValidatingItemProcessor() {
		ValidatingItemProcessor<Customer> itemProcessor = new ValidatingItemProcessor<>(validator());

		itemProcessor.setFilter(true);

		return itemProcessor;
	}

	
	public ItemProcessorAdapter<Customer, Customer> upperCaseItemProcessor(UpperCaseNameService service) {
		ItemProcessorAdapter<Customer, Customer> adapter = new ItemProcessorAdapter<>();

		adapter.setTargetObject(service);
		adapter.setTargetMethod("upperCase");

		return adapter;
	}

	
	@StepScope
	public ScriptItemProcessor<Customer, Customer> lowerCaseItemProcessor(
			@Value("#{jobParameters['script']}") Resource script) {

		ScriptItemProcessor<Customer, Customer> itemProcessor =
				new ScriptItemProcessor<>();

		itemProcessor.setScript(script);

		return itemProcessor;
	}

	
	public CompositeItemProcessor<Customer, Customer> itemProcessor() {
		CompositeItemProcessor<Customer, Customer> itemProcessor =
				new CompositeItemProcessor<>();

		itemProcessor.setDelegates(Arrays.asList(
				customerValidatingItemProcessor(),
				upperCaseItemProcessor(null),
				lowerCaseItemProcessor(null)));

		return itemProcessor;
	}

	
	public Step copyFileStep() {

		return this.stepBuilderFactory.get("CompositeItemProcessorJob-copyFileStep")
				.<Customer, Customer>chunk(5)
				.reader(customerItemReader())
				.processor(itemProcessor())
				.writer(itemWriter())
				.build();
	}

	
	public Job getJob(Map<String, String> params)  {

		return this.jobBuilderFactory.get("CompositeItemProcessorJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())
				.build();
	}

}

