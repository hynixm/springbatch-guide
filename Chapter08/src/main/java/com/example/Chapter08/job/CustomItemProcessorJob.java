package com.example.Chapter08.job;

import com.example.Chapter08.batch.EvenFilteringItemProcessor;
import com.example.Chapter08.domain.Customer;
import com.example.Chapter08.job.domain.AbstractBatchJob;
import com.example.Chapter08.util.CacheUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CustomItemProcessorJob extends AbstractBatchJob {
	

	public FlatFileItemReader<Customer> customerItemReader() {
		String inputFilePath = (String) CacheUtil.getInstance().get("CustomItemProcessorJob");
		FileSystemResource inputFile = new FileSystemResource(inputFilePath);

		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.delimited()
				.names(new String[] {"firstName",
						"middleInitial",
						"lastName",
						"address",
						"city",
						"state",
						"zip"})
				.targetType(Customer.class)
				.resource(inputFile)
				.build();
	}

	
	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}

	
	public EvenFilteringItemProcessor itemProcessor() {
		return new EvenFilteringItemProcessor();
	}

	
	public Step copyFileStep() {

		return this.stepBuilderFactory.get("CustomItemProcessorJob-copyFileStep")
				.<Customer, Customer>chunk(5)
				.reader(customerItemReader())
				.processor(itemProcessor())
				.writer(itemWriter())
				.build();
	}

	
	public Job getJob(Map<String, String> params) {

		return this.jobBuilderFactory.get("CustomItemProcessorJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())
				.build();
	}

}

