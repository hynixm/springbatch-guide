package com.example.Chapter08.job.domain;

import com.example.Chapter08.util.CacheUtil;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

public class JobListener {

    @BeforeJob
    public void beforJob(JobExecution jobExecution){
        String jobName = jobExecution.getJobInstance().getJobName();
        //本来缓存工具用来传参数的，这里用来判断是否有一个同名的job正在运行
        if(CacheUtil.getInstance().containsKey(jobName)){
        //if(jobExecution.isRunning()){// 也可以使用springbatch框架方法（job是停止或者失败状态，这里就不好使了）
            String info = "job[" + jobName + "]已经在执行，请稍后再执行";
            System.out.println(info);
            jobExecution.stop();
        }else{
            CacheUtil.getInstance().put(jobName,"job is running");
        }
    }

    @AfterJob
    public void afterJob(JobExecution jobExecution){
        CacheUtil.getInstance().removeCacheData(jobExecution.getJobInstance().getJobName());
    }
}
