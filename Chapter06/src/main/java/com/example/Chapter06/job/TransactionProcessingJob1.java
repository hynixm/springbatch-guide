package com.example.Chapter06.job;

import com.example.Chapter06.batch.TransactionApplierProcessor;
import com.example.Chapter06.batch.TransactionReader;
import com.example.Chapter06.domain.AbstractBatchJob;
import com.example.Chapter06.domain.AccountSummary;
import com.example.Chapter06.domain.Transaction;
import com.example.Chapter06.domain.TransactionDao;
import com.example.Chapter06.domain.support.TransactionDaoSupport;
import com.example.Chapter06.util.CacheUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.PassThroughFieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.net.MalformedURLException;

/**
 * 改造 TransactionProcessingJob ：job参数加载到缓存CacheUtil中，在step中可以取用
 */
@Component
public class TransactionProcessingJob1 extends AbstractBatchJob {
    @Autowired
    private DataSource dataSource;

    // 自定义的流读取器（继承了ItemStreamReader），将文件读取器对象 fileItemReader 了封装进去
    public TransactionReader transactionReader() {
        Resource transactionFile = new ClassPathResource((String) CacheUtil.getInstance().getCacheData("transactionFile"),this.getClass().getClassLoader());
        return new TransactionReader(fileItemReader(transactionFile));
    }

    // step1-read 文件读取器
    public FlatFileItemReader<FieldSet> fileItemReader(Resource inputFile) {
        return new FlatFileItemReaderBuilder<FieldSet>()
                .name("fileItemReader")
                .resource(inputFile)
                .lineTokenizer(new DelimitedLineTokenizer())
                .fieldSetMapper(new PassThroughFieldSetMapper())
                .build();
    }

    //step1-write 写入器，将文件读取的数据插入 TRANSACTION 表
    public JdbcBatchItemWriter<Transaction> transactionWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Transaction>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                /*.sql("INSERT INTO TRANSACTION " +
                        "(ACCOUNT_SUMMARY_ID, TIMESTAMP, AMOUNT) " +
                        "VALUES ((SELECT ID FROM ACCOUNT_SUMMARY " +
                        "	WHERE ACCOUNT_NUMBER = :accountNumber), " +
                        ":timestamp, :amount)")*/
                .sql("insert into transaction (ACCOUNT_SUMMARY_ID, TIMESTAMP, AMOUNT) values(:accountNumber,:timestamp,:amount)")
                .dataSource(dataSource)
                .build();
    }

    /**
     * 第1步，将csv文件数据读取，插入数据表中
     * @return
     */
    public Step importTransactionFileStep() {
        JdbcBatchItemWriter<Transaction> transactionJdbcBatchItemWriter = transactionWriter(dataSource);
        //在spring的bean的生命周期中，实例化->生成对象->属性填充后会进行afterPropertiesSet方法，这个方法可以用在一些特殊情况中，也就是某个对象的某个属性需要经过外界得到
        //TransactionProcessingJob.java的代码中，JdbcBatchItemWriter是作为组件加载到spring容器中的，会自动调用这个方法。
        //JdbcBatchItemWriter#afterPropertiesSet 方法有特殊逻辑，不执行这里要报错（其他job、step、read、processor、writer 一样要去注意一下）
        transactionJdbcBatchItemWriter.afterPropertiesSet();
        return this.stepBuilderFactory.get("importTransactionFileStep")
                .<Transaction, Transaction>chunk(100)
                .reader(transactionReader())
                //.processor(new ImportTransactionFileStepProcess())
                .writer(transactionJdbcBatchItemWriter)
                //.allowStartIfComplete(true)// 作业上一次执行是失败或者停止，第二次执行会，步骤会重新执行
                //.listener(transactionReader())
                .build();
    }


    // step2-read 读取账户当前余额
    // step3-read 读取的刷新后的账户余额，同样的逻辑，复用逻辑，但是是两个实例（@StepScope 延迟加载）
    public JdbcCursorItemReader<AccountSummary> accountSummaryReader(DataSource dataSource) {
        return new JdbcCursorItemReaderBuilder<AccountSummary>()
                .name("accountSummaryReader")
                .dataSource(dataSource)
                .sql("SELECT ACCOUNT_NUMBER, CURRENT_BALANCE " +
                        "FROM ACCOUNT_SUMMARY A " +
                        "WHERE A.ID IN (" +
                        "	SELECT DISTINCT T.ACCOUNT_SUMMARY_ID " +
                        "	FROM TRANSACTION T) " +
                        "ORDER BY A.ACCOUNT_NUMBER")
                .rowMapper((resultSet, rowNumber) -> { //sql查询的结果数据封装到AccountSummary对象
                    AccountSummary summary = new AccountSummary();

                    summary.setAccountNumber(resultSet.getString("account_number"));
                    summary.setCurrentBalance(resultSet.getDouble("current_balance"));

                    return summary;
                }).build();
    }

    public TransactionDao transactionDao(DataSource dataSource) {
        return new TransactionDaoSupport(dataSource);
    }

    //step2-process 查询账户对应的流水，将流水所有交易额汇总，然后加上step2-read中的账户余额
    //简单的计算，拆的很零散
    public TransactionApplierProcessor transactionApplierProcessor() {
        return new TransactionApplierProcessor(transactionDao(dataSource));
    }

    // step2-write 更新账户表的余额
    public JdbcBatchItemWriter<AccountSummary> accountSummaryWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<AccountSummary>()
                .dataSource(dataSource)
                .itemSqlParameterSourceProvider(
                        new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("UPDATE ACCOUNT_SUMMARY " +
                        "SET CURRENT_BALANCE = :currentBalance " +
                        "WHERE ACCOUNT_NUMBER = :accountNumber")
                .build();
    }

    // 第2步 刷新账户表余额
    public Step applyTransactionsStep() {
        return this.stepBuilderFactory.get("applyTransactionsStep")
                .<AccountSummary, AccountSummary>chunk(100)
                .reader(accountSummaryReader(dataSource))
                .processor(transactionApplierProcessor())
                .writer(accountSummaryWriter(dataSource))
                .build();
    }

    // step3-write
    public FlatFileItemWriter<AccountSummary> accountSummaryFileWriter(Resource summaryFile) {

        DelimitedLineAggregator<AccountSummary> lineAggregator = new DelimitedLineAggregator<>();
        BeanWrapperFieldExtractor<AccountSummary> fieldExtractor = new BeanWrapperFieldExtractor<>();
        fieldExtractor.setNames(new String[] {"accountNumber", "currentBalance"});
        fieldExtractor.afterPropertiesSet();
        lineAggregator.setFieldExtractor(fieldExtractor);

        return new FlatFileItemWriterBuilder<AccountSummary>()
                .name("accountSummaryFileWriter")
                .resource(summaryFile)
                .shouldDeleteIfExists(true)
                .lineAggregator(lineAggregator)
                .build();
    }

    // 第3步 刷新后的账户余额数据导出到文件
    public Step generateAccountSummaryStep() throws MalformedURLException {
        //Resource summaryFile = new ClassPathResource((String) CacheUtil.getInstance().getCacheData("summaryFile"),this.getClass().getClassLoader());
        FileUrlResource summaryFile = new FileUrlResource((String) CacheUtil.getInstance().getCacheData("summaryFile"));
        return this.stepBuilderFactory.get("generateAccountSummaryStep")
                .<AccountSummary, AccountSummary>chunk(100)
                .reader(accountSummaryReader(dataSource))
                .writer(accountSummaryFileWriter(summaryFile))
                .build();
    }

    public Job getJob() throws MalformedURLException {
/*		return this.jobBuilderFactory.get("transactionJob1")
				.incrementer(new RunIdIncrementer())
				.preventRestart()
				.start(importTransactionFileStep())
				.next(applyTransactionsStep())
				.next(generateAccountSummaryStep())
				.build();*/
        return this.jobBuilderFactory.get("transactionJob1")
                .start(importTransactionFileStep())
                .on("STOPPED").stopAndRestart(importTransactionFileStep())
                .from(importTransactionFileStep()).on("*").to(applyTransactionsStep())
                .from(applyTransactionsStep()).next(generateAccountSummaryStep())
                .end()
                .build();

        /*return this.jobBuilderFactory.get("transactionJob1")
                //.preventRestart() //作业失败后，不能使用同样的参数重启
                .start(importTransactionFileStep())
                //.next(applyTransactionsStep())
                //.next(generateAccountSummaryStep())
                .build();*/
    }
}
