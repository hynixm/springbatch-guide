/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter06.batch;

import com.example.Chapter06.domain.Transaction;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.file.transform.FieldSet;

import java.text.SimpleDateFormat;

/**
 * 把读取器，监听器放在一个类中了
 * 流读取器 实现接口ItemStreamReader
 * @AfterStep 、 @BeforStep 步骤监听器
 */
public class TransactionReader implements ItemStreamReader<Transaction> {

	private ItemStreamReader<FieldSet> fieldSetReader;
	private int recordCount = 0;
	private int expectedRecordCount = 0;

	private StepExecution stepExecution;

	public TransactionReader(ItemStreamReader<FieldSet> fieldSetReader) {
		this.fieldSetReader = fieldSetReader;
	}

	public Transaction read() throws Exception {
		return process(fieldSetReader.read());
	}

	private Transaction process(FieldSet fieldSet) {
	/*	if(this.recordCount == 25) {
			throw new ParseException("This isn't what I hoped to happen");
		}*/

		Transaction result = null;

		if(fieldSet != null) {
			if(fieldSet.getFieldCount() > 1) {// transactionFile.csv文件数据，最后一条数据只有一个field，是数据量
				result = new Transaction();
				result.setAccountNumber(fieldSet.readString(0));
				result.setTimestamp(
						fieldSet.readDate(1,
								"yyyy-MM-DD HH:mm:ss"));
				result.setAmount(fieldSet.readDouble(2));

				recordCount++;
			} else {
				expectedRecordCount = fieldSet.readInt(0);//文件中最后一行记录的数据量

				/*if(expectedRecordCount != this.recordCount) {
					// stepExecution 赋值，在@BeforeStep 步骤监听中
					this.stepExecution.setTerminateOnly();// 设置标识，完成后就结束当前步骤
				}*/
			}
		}

		return result;
	}

	public void setFieldSetReader(ItemStreamReader<FieldSet> fieldSetReader) {
		this.fieldSetReader = fieldSetReader;
	}

	// 步骤监听器，如果最后数据量不对，则步骤状态为STOPPED
	@AfterStep
	public ExitStatus afterStep(StepExecution execution) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf.format(execution.getStartTime()) + "---------------------" + execution.getStepName() + execution.getReadCount() +"---" + recordCount + "=" + expectedRecordCount);


		if(recordCount == expectedRecordCount) {
			return execution.getExitStatus();
		} else {
			return ExitStatus.STOPPED;
		}
	}

	@BeforeStep
	public void beforeStep(StepExecution execution) {
		System.out.println("beforeStep---------------------" + execution.getStepName());
		this.stepExecution = execution;
	}

	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		this.fieldSetReader.open(executionContext);
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		this.fieldSetReader.update(executionContext);
	}

	@Override
	public void close() throws ItemStreamException {
		this.fieldSetReader.close();
	}
}
