package com.example.Chapter06;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@EnableBatchProcessing
@SpringBootApplication
public class SpringBatchApp {
    public static void main(String[] args){
        ApplicationContext ctx = SpringApplication.run(SpringBatchApp.class, args);
        String[] beanNames = ctx.getBeanDefinitionNames();
        System.out.println("beanSize:" + beanNames.length);
        int i = 0;
        for(String beanName : beanNames){
            System.out.println(String.format("%d  %s",++i,beanName));
        }
    }
}
