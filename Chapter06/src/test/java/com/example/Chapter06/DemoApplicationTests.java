package com.example.Chapter06;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class DemoApplicationTests {

	private RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception{
		this.restTemplate = new RestTemplate();
	}

	@Test
	public void restApplicationJobTest()  {
		String url="http://localhost:8080/run";

		HttpHeaders headers = new HttpHeaders();
		//定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
		headers.setContentType(MediaType.APPLICATION_JSON);

		//RestTemplate带参传的时候要用HttpEntity<?>对象传递
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "restApplicationJob");
		Map<String, String> mapParams = new HashMap<String, String>();
		mapParams.put("job","1");
		map.put("jobParameters",mapParams);

		HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

		ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
		//获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
		System.out.println(entity.getBody());
	}

	@Test
	public void transactionJobTest()  {
		String url="http://localhost:8080/jobname/run";

		HttpHeaders headers = new HttpHeaders();
		//定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
		headers.setContentType(MediaType.APPLICATION_JSON);

		//RestTemplate带参传的时候要用HttpEntity<?>对象传递
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "transactionJob");
		Map<String, String> mapParams = new HashMap<String, String>();
		mapParams.put("transactionFile","classpath:input/transactionFile.csv");
		mapParams.put("summaryFile","file:summaryFile.txt");
		mapParams.put("job","job7");
		map.put("jobParameters",mapParams);

		HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

		ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
		//获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
		System.out.println(entity.getBody());
	}

	// job参数非自增调用，测试失败停止重新调用
	@Test
	public void transactionJob1Test()  {
		String url="http://localhost:8080/jobname/run";

		HttpHeaders headers = new HttpHeaders();
		//定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
		headers.setContentType(MediaType.APPLICATION_JSON);

		//RestTemplate带参传的时候要用HttpEntity<?>对象传递
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "transactionProcessingJob1");
		//map.put("name", "transactionJob");
		Map<String, String> mapParams = new HashMap<String, String>();
		//mapParams.put("transactionFile","classpath:input/transactionFile.csv");
		//mapParams.put("summaryFile","file:summaryFile.txt");
		mapParams.put("transactionFile","input/transactionFile.csv");
		mapParams.put("summaryFile","input/summaryFile.txt");

		mapParams.put("job","job1");
		map.put("jobParameters",mapParams);

		HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

		ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
		//获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
		System.out.println(entity.getBody());
	}

}

