package com.example.Chapter07;

import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SimpleTest {

    @Test
    public void test() throws IOException {
        Resource inputFileResource = new FileSystemResource("input/cusomer.txt");
        System.out.println(inputFileResource.exists());
        System.out.println(inputFileResource.getFile().getAbsoluteFile());
    }

    @Test
    public void jdbcCursorJobTest(){
        RestTemplate restTemplate = new RestTemplate();
        String url="http://localhost:8080/springbatch7/jdbcCursorJobJob/runs";

        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("city", "Hartford");

        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(map, headers);

        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串
        System.out.println(entity.getBody());
    }
}
