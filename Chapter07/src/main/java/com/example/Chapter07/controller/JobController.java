package com.example.Chapter07.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.Chapter07.job.domain.BatchJob;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

//@RestController 是@controller和@ResponseBody 的结合
//@ResponseBody 它的作用简短截说就是指该类中所有的API接口返回的数据，甭管你对应的方法返回Map或是其他Object，它会以Json字符串的形式返回给客户端
@RestController
@RequestMapping("/springbatch7")
public class JobController {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String test() throws Exception {
        return "Welcome learn springbatch";
    }


    /**
     * 无参执行job
     */
    @RequestMapping(value = "/{jobName}/run")
    public ExitStatus runJob(@PathVariable("jobName") String jobName) throws Exception {
        Map<String, JobParameter> parameterMap = new HashMap<>();
        JobParameters jobParameters = new JobParameters(parameterMap); // job参数
        // 实现BatchJob接口的组件的getJob方法获取job
        Job job = this.context.getBean(jobName, BatchJob.class).getJob();

        return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }

    /**
     * 指定参数执行job
     */
    @RequestMapping(value = "/run")
    public ExitStatus runJobOfParams(@Param("jobName") String jobName,@Param("customerFile") String customerFile) throws Exception {
        Map<String, JobParameter> parameterMap = new HashMap<>();

        parameterMap.put("customerFile", new JobParameter(customerFile, true)); //识别性参数（每次输入的这个值不同，就可以执行）
        //parameterMap.put(jobName, new JobParameter(jobName, false)); //非识别性参数

        JobParameters jobParameters = new JobParameters(parameterMap); // job参数

        // job 已经作为spring的组件，直接获取
        Job job = this.context.getBean(jobName, Job.class);

        JobParameters jobParametersAdd =
                new JobParametersBuilder(jobParameters,
                        this.jobExplorer)
                        .getNextJobParameters(job)// job中封装了JobParametersIncrementer的实现类的实例化对象（使用incrementer方法），这里利用这个对象给job封装一个自动增量或者变化的识别性参数
                        .toJobParameters();
        return this.jobLauncher.run(job, jobParametersAdd).getExitStatus();
    }

    /**
     * 灵活参数，post请求-json格式
     * @RequestBody主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)；而最常用的使用请求体传参的无疑是POST请求了，
     * 所以使用@RequestBody接收数据时，一般都用POST方式进行提交。在后端的同一个接收方法里，@RequestBody与@RequestParam()可以同时使用，
     * @RequestBody最多只能有一个，而@RequestParam()可以有多个。
     *
     * @RequestBody修饰的后端参数可以是一个 字符串 也可以是一个对象，如果是对象，必须属性名称和类型要映射json的内容
     */
    @RequestMapping(value = "/{jobName}/runs",method =RequestMethod.POST )
    public ExitStatus runJobParamAutoAdd(@PathVariable("jobName") String jobName,@RequestBody String params) throws Exception {
        Map<String, JobParameter> parameterMap = new HashMap<>();
        parameterMap.put("params", new JobParameter(params, true));

        JobParameters jobParameters = new JobParameters(parameterMap); // job参数
        Job job;
        if(jobName.endsWith("JobJob")){
            // job 已经作为spring的组件，直接获取
            job = this.context.getBean(jobName, Job.class);
        }else{
            // 组件方法获取job
            job = this.context.getBean(jobName, BatchJob.class).getJob();
        }

        JobParameters jobParametersAdd =
                new JobParametersBuilder(jobParameters,
                        this.jobExplorer)
                        .getNextJobParameters(job)// job中封装了JobParametersIncrementer的实现类的实例化对象（使用incrementer方法），这里利用这个对象给job封装一个自动增量或者变化的识别性参数
                        .toJobParameters();

        return this.jobLauncher.run(job, jobParametersAdd).getExitStatus();
    }


    /**
     * 灵活参数，名称映射
     * @RequestParam（）指定的参数可以是普通元素、数组、集合、对象等等
     */
    @RequestMapping(value = "/{jobName}/map/run")
    public ExitStatus runJobMapParamAutoAdd(@PathVariable("jobName") String jobName,@RequestParam Map<String,String> params) throws Exception {
        Map<String, JobParameter> parameterMap = new HashMap<>();
        parameterMap.put("params", new JobParameter(JSONObject.toJSONString(params), true));

        JobParameters jobParameters = new JobParameters(parameterMap); // job参数
        Job job;
        if(jobName.endsWith("JobJob")){
            // job 已经作为spring的组件，直接获取
            job = this.context.getBean(jobName, Job.class);
        }else{
            // 组件方法获取job
            job = this.context.getBean(jobName, BatchJob.class).getJob();
        }

        JobParameters jobParametersAdd =
                new JobParametersBuilder(jobParameters,
                        this.jobExplorer)
                        .getNextJobParameters(job)// job中封装了JobParametersIncrementer的实现类的实例化对象（使用incrementer方法），这里利用这个对象给job封装一个自动增量或者变化的识别性参数
                        .toJobParameters();

        return this.jobLauncher.run(job, jobParametersAdd).getExitStatus();
    }
}
