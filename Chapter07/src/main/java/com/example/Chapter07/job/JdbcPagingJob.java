package com.example.Chapter07.job;

import com.alibaba.fastjson.JSON;
import com.example.Chapter07.domain.Customer;
import com.example.Chapter07.domain.CustomerRowMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Map;

/**
 * http://localhost:8080/springbatch7/jdbcCursorJobJob/map/run?city=Cincinnati
 */
@Configuration
public class JdbcPagingJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

	// JdbcPagingItemReader jdbc分页读取器
	@Bean(name="jdbcPagingJobItemReader")
	@StepScope
	public JdbcPagingItemReader<Customer> customerItemReader(PagingQueryProvider queryProvider, @Value("#{jobParameters['params']}") String params) throws Exception {
		Map<String, Object> parameterValues = JSON.parseObject(params);

		return new JdbcPagingItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.queryProvider(pagingQueryProvider().getObject()) //分页查询
				.parameterValues(parameterValues)
				.pageSize(10)
				.rowMapper(new CustomerRowMapper())
				.build();
	}

	//数据库分页查询对象工厂，需要设置数据源，工厂类自动确认数据库类型
	@Bean
	public SqlPagingQueryProviderFactoryBean pagingQueryProvider() {
		SqlPagingQueryProviderFactoryBean factoryBean = new SqlPagingQueryProviderFactoryBean();

		factoryBean.setDataSource(dataSource);//设置数据用于确定数据库类型，然后factoryBean.getObject() 才能获取正确的数据库分页查询对象PagingQueryProvider
		factoryBean.setSelectClause("select *");
		factoryBean.setFromClause("from Customer");
		factoryBean.setWhereClause("where city = :city");// 这里参数可以用名称映射
		factoryBean.setSortKey("lastName"); // 用于order by,必须设置

		return factoryBean;
	}

	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}

	public Step copyFileStep() throws Exception {
		return this.stepBuilderFactory.get("jdbcPagingJobJob-copyFileStep")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader(null, null))
				.writer(itemWriter())
				.build();
	}

	@Bean(name = "jdbcPagingJobJob")
	public Job job() throws Exception {
		return this.jobBuilderFactory.get("jdbcPagingJobJob")
				.start(copyFileStep())
                .incrementer(new RunIdIncrementer())
				.build();
	}



}

