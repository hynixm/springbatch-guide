package com.example.Chapter07.job;

import com.example.Chapter07.batch.CustomerFieldSetMapper;
import com.example.Chapter07.domain.Customer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * http://localhost:8080/springbatch7/run?jobName=delimitedJobJob&customerFile=Chapter07/input/customer.txt
 */
@Configuration
public class DelimitedJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	/**
	 * @StepScope配合@Value("#{jobParameters['customerFile']}")  从job的启动参数中获取所需参数
	 * @StepScope只能用在最底层处理单位(reader,processor,writer,tasklet)的方法上,配合@Bean使用
	 * 被@StepScope注解修饰的bean只会在job中step启动时进行初始化,step处理完成后便会被销毁
	 *
	 */
	//分割符文件 读取器
	// http://localhost:8080/springbatch7/run?jobName=delimitedJobJob&customerFile=Chapter07/input/customer1.txt
	@Bean
	@StepScope
	public FlatFileItemReader<Customer> customerItemReader(@Value("#{jobParameters['customerFile']}") String inputFilePath) {
		Resource inputFileResource = new FileSystemResource(inputFilePath);
		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")//构建的读取器名称
				.delimited() //默认分隔符是 ,
				.quoteCharacter('#') // #代表单引号,括起来的逗号做为字符串内容解析
				.names(new String[] {"firstName",
						"middleInitial",
						"lastName",
						"address",
						"city",
						"state",
						"zipCode"})
				.fieldSetMapper(new CustomerFieldSetMapper())
				.resource(inputFileResource)
				.build();
	}

	//7-19 自定义记录解析（将 adress和street 内容合在一起）
/*
	@Bean
	@StepScope
	public FlatFileItemReader<Customer> customerItemReader(@Value("#{jobParameters['customerFile']}")String inputFilePath) {
		Resource inputFileResource = new FileSystemResource(inputFilePath);
		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.lineTokenizer(new CustomerFileLineTokenizer()) //自定义记录解析
				.fieldSetMapper(new CustomerFieldSetMapper())//域映射
				.resource(inputFileResource)
				.build();
	}
*/


	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}


	@Bean
	public Step copyFileStep() {
		return this.stepBuilderFactory.get("delimitedJobJob-copyFileStep")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader(null))
				.writer(itemWriter())
				.build();
	}

	@Bean(name="delimitedJobJob")
	public Job job() {
		return this.jobBuilderFactory.get("delimitedJobJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())//配合启动job的JobParameters设置，这里添加一个job识别参数，每次运行自增1
				.build();
	}



}


