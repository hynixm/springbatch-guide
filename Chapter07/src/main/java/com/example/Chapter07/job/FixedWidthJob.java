package com.example.Chapter07.job;

import com.example.Chapter07.domain.Customer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * http://localhost:8080/springbatch7/run?jobName=fixedWidthJobJob&customerFile=Chapter07/input/customerFixedWidth.txt
 * FlatFileItemReader 平面文件读取器，固定宽度field
 */
@Configuration
public class FixedWidthJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean(name="fixedWidthJobCustomerItemReader")//为了避免同名的bean
	@StepScope//延迟加载bean，job传参
	public FlatFileItemReader<Customer> customerItemReader(
			@Value("#{jobParameters['customerFile']}") String inputFilePath) {
		Resource inputFileResource = new FileSystemResource(inputFilePath);
		return new FlatFileItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.resource(inputFileResource)
				.fixedLength()
				.columns(new Range[]{new Range(1,11), new Range(12, 12), new Range(13, 22),
						new Range(23, 26), new Range(27,46), new Range(47,62), new Range(63,64),
						new Range(65,69)})//每一列固定宽度的开始结束索引，Rang数组
				.names(new String[] {"firstName", "middleInitial", "lastName",
						"addressNumber", "street", "city", "state","zipCode"})//内个域对应的字段名称
				.targetType(Customer.class)//一行数据映射的pojo对象
				.build();
	}


	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}


	public Step copyFileStep() {
		return this.stepBuilderFactory.get("fixedWidthJobJob-copyFileStep")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader(null))
				.writer(itemWriter())
				.build();
	}

	@Bean(name="fixedWidthJobJob")
	public Job job() {
		return this.jobBuilderFactory.get("fixedWidthJobJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())//配合启动job的JobParameters设置，这里添加一个job识别参数，每次运行自增1
				.build();
	}

}

