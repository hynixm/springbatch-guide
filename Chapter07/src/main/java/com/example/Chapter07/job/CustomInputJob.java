package com.example.Chapter07.job;

import com.example.Chapter07.batch.CustomerItemListener;
import com.example.Chapter07.batch.CustomerItemReader;
import com.example.Chapter07.domain.Customer;
import com.example.Chapter07.job.domain.BatchJob;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//job组装功能类作为bean组件加载到容器，并且实现统一的接口，可以获取 Job对象
//http://localhost:8080/springbatch7/customInputJob/run
@Component
public class CustomInputJob implements BatchJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	// CustomerItemReader 7.6 自定义输入
	public CustomerItemReader customerItemReader() {
		CustomerItemReader customerItemReader = new CustomerItemReader();

		customerItemReader.setName("customerItemReader");

		return customerItemReader;
	}

	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}

	public Step copyFileStep() {
		return this.stepBuilderFactory.get("CustomInputJob-copyFileStep")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader())
				.writer(itemWriter())
				.listener(new CustomerItemListener()) //读取监听器，注解方法@OnReadError
				.build();
	}

	public Job getJob() {
		return this.jobBuilderFactory.get("customInputJob")
				.start(copyFileStep())
				.build();
	}


}

