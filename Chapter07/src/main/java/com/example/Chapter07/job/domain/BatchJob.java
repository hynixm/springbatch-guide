package com.example.Chapter07.job.domain;

import org.springframework.batch.core.Job;

import java.net.MalformedURLException;

public interface BatchJob {

    Job getJob() throws MalformedURLException;
}
