package com.example.Chapter07.job;

import com.alibaba.fastjson.JSON;
import com.example.Chapter07.domain.Customer;
import com.example.Chapter07.domain.CustomerRowMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;

import javax.sql.DataSource;
import java.util.Map;

@Configuration
public class JdbcCursorJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private DataSource dataSource;

	// JdbcCursorItemReader jdbc游标读取数据
	// springbatch 每次调用read()时，数据将返回一行记录
	// 数据量大，每行一个请求，请求网络开销大。
	// 非线程安全，多线程不能用
	public JdbcCursorItemReader<Customer> customerItemReader() {
		return new JdbcCursorItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.sql("select * from customer where city = ?")
				.rowMapper(new CustomerRowMapper())
				.preparedStatementSetter(citySetter(null))
				.build();
	}

	@Bean
	@StepScope //延迟加载
	public ArgumentPreparedStatementSetter citySetter(
			@Value("#{jobParameters['params']}") String params) {

		Map<String,Object> map = JSON.parseObject(params, Map.class);
		String city = (String) map.get("city");
		//sql 语句的参数，按照?顺序匹配params
		return new ArgumentPreparedStatementSetter(new Object [] {city});
	}


	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}


	public Step copyFileStep() {
		return this.stepBuilderFactory.get("jdbcCursorJobJob-copyFileStep")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader())
				.writer(itemWriter())
				.build();
	}

	@Bean(name="jdbcCursorJobJob")
	public Job job() {
		return this.jobBuilderFactory.get("jdbcCursorJobJob")
				.start(copyFileStep())
				.incrementer(new RunIdIncrementer())//自增参数 run.id
				.build();
	}



}

